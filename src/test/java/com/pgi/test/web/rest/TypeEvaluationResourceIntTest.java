package com.pgi.test.web.rest;

import com.pgi.test.TestApp;

import com.pgi.test.domain.TypeEvaluation;
import com.pgi.test.repository.TypeEvaluationRepository;
import com.pgi.test.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.pgi.test.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pgi.test.domain.enumeration.EnumTypeEvaluation;
/**
 * Test class for the TypeEvaluationResource REST controller.
 *
 * @see TypeEvaluationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApp.class)
public class TypeEvaluationResourceIntTest {

    private static final EnumTypeEvaluation DEFAULT_TYPE_EVAL = EnumTypeEvaluation.LMD;
    private static final EnumTypeEvaluation UPDATED_TYPE_EVAL = EnumTypeEvaluation.HYBRIDE;

    @Autowired
    private TypeEvaluationRepository typeEvaluationRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTypeEvaluationMockMvc;

    private TypeEvaluation typeEvaluation;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TypeEvaluationResource typeEvaluationResource = new TypeEvaluationResource(typeEvaluationRepository);
        this.restTypeEvaluationMockMvc = MockMvcBuilders.standaloneSetup(typeEvaluationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TypeEvaluation createEntity(EntityManager em) {
        TypeEvaluation typeEvaluation = new TypeEvaluation()
            .typeEval(DEFAULT_TYPE_EVAL);
        return typeEvaluation;
    }

    @Before
    public void initTest() {
        typeEvaluation = createEntity(em);
    }

    @Test
    @Transactional
    public void createTypeEvaluation() throws Exception {
        int databaseSizeBeforeCreate = typeEvaluationRepository.findAll().size();

        // Create the TypeEvaluation
        restTypeEvaluationMockMvc.perform(post("/api/type-evaluations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(typeEvaluation)))
            .andExpect(status().isCreated());

        // Validate the TypeEvaluation in the database
        List<TypeEvaluation> typeEvaluationList = typeEvaluationRepository.findAll();
        assertThat(typeEvaluationList).hasSize(databaseSizeBeforeCreate + 1);
        TypeEvaluation testTypeEvaluation = typeEvaluationList.get(typeEvaluationList.size() - 1);
        assertThat(testTypeEvaluation.getTypeEval()).isEqualTo(DEFAULT_TYPE_EVAL);
    }

    @Test
    @Transactional
    public void createTypeEvaluationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = typeEvaluationRepository.findAll().size();

        // Create the TypeEvaluation with an existing ID
        typeEvaluation.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTypeEvaluationMockMvc.perform(post("/api/type-evaluations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(typeEvaluation)))
            .andExpect(status().isBadRequest());

        // Validate the TypeEvaluation in the database
        List<TypeEvaluation> typeEvaluationList = typeEvaluationRepository.findAll();
        assertThat(typeEvaluationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTypeEvalIsRequired() throws Exception {
        int databaseSizeBeforeTest = typeEvaluationRepository.findAll().size();
        // set the field null
        typeEvaluation.setTypeEval(null);

        // Create the TypeEvaluation, which fails.

        restTypeEvaluationMockMvc.perform(post("/api/type-evaluations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(typeEvaluation)))
            .andExpect(status().isBadRequest());

        List<TypeEvaluation> typeEvaluationList = typeEvaluationRepository.findAll();
        assertThat(typeEvaluationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTypeEvaluations() throws Exception {
        // Initialize the database
        typeEvaluationRepository.saveAndFlush(typeEvaluation);

        // Get all the typeEvaluationList
        restTypeEvaluationMockMvc.perform(get("/api/type-evaluations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(typeEvaluation.getId().intValue())))
            .andExpect(jsonPath("$.[*].typeEval").value(hasItem(DEFAULT_TYPE_EVAL.toString())));
    }

    @Test
    @Transactional
    public void getTypeEvaluation() throws Exception {
        // Initialize the database
        typeEvaluationRepository.saveAndFlush(typeEvaluation);

        // Get the typeEvaluation
        restTypeEvaluationMockMvc.perform(get("/api/type-evaluations/{id}", typeEvaluation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(typeEvaluation.getId().intValue()))
            .andExpect(jsonPath("$.typeEval").value(DEFAULT_TYPE_EVAL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTypeEvaluation() throws Exception {
        // Get the typeEvaluation
        restTypeEvaluationMockMvc.perform(get("/api/type-evaluations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTypeEvaluation() throws Exception {
        // Initialize the database
        typeEvaluationRepository.saveAndFlush(typeEvaluation);
        int databaseSizeBeforeUpdate = typeEvaluationRepository.findAll().size();

        // Update the typeEvaluation
        TypeEvaluation updatedTypeEvaluation = typeEvaluationRepository.findOne(typeEvaluation.getId());
        // Disconnect from session so that the updates on updatedTypeEvaluation are not directly saved in db
        em.detach(updatedTypeEvaluation);
        updatedTypeEvaluation
            .typeEval(UPDATED_TYPE_EVAL);

        restTypeEvaluationMockMvc.perform(put("/api/type-evaluations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTypeEvaluation)))
            .andExpect(status().isOk());

        // Validate the TypeEvaluation in the database
        List<TypeEvaluation> typeEvaluationList = typeEvaluationRepository.findAll();
        assertThat(typeEvaluationList).hasSize(databaseSizeBeforeUpdate);
        TypeEvaluation testTypeEvaluation = typeEvaluationList.get(typeEvaluationList.size() - 1);
        assertThat(testTypeEvaluation.getTypeEval()).isEqualTo(UPDATED_TYPE_EVAL);
    }

    @Test
    @Transactional
    public void updateNonExistingTypeEvaluation() throws Exception {
        int databaseSizeBeforeUpdate = typeEvaluationRepository.findAll().size();

        // Create the TypeEvaluation

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTypeEvaluationMockMvc.perform(put("/api/type-evaluations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(typeEvaluation)))
            .andExpect(status().isCreated());

        // Validate the TypeEvaluation in the database
        List<TypeEvaluation> typeEvaluationList = typeEvaluationRepository.findAll();
        assertThat(typeEvaluationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTypeEvaluation() throws Exception {
        // Initialize the database
        typeEvaluationRepository.saveAndFlush(typeEvaluation);
        int databaseSizeBeforeDelete = typeEvaluationRepository.findAll().size();

        // Get the typeEvaluation
        restTypeEvaluationMockMvc.perform(delete("/api/type-evaluations/{id}", typeEvaluation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TypeEvaluation> typeEvaluationList = typeEvaluationRepository.findAll();
        assertThat(typeEvaluationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TypeEvaluation.class);
        TypeEvaluation typeEvaluation1 = new TypeEvaluation();
        typeEvaluation1.setId(1L);
        TypeEvaluation typeEvaluation2 = new TypeEvaluation();
        typeEvaluation2.setId(typeEvaluation1.getId());
        assertThat(typeEvaluation1).isEqualTo(typeEvaluation2);
        typeEvaluation2.setId(2L);
        assertThat(typeEvaluation1).isNotEqualTo(typeEvaluation2);
        typeEvaluation1.setId(null);
        assertThat(typeEvaluation1).isNotEqualTo(typeEvaluation2);
    }
}
