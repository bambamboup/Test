package com.pgi.test.web.rest;

import com.pgi.test.TestApp;

import com.pgi.test.domain.Classe;
import com.pgi.test.domain.Formation;
import com.pgi.test.repository.ClasseRepository;
import com.pgi.test.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static com.pgi.test.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pgi.test.domain.enumeration.EnumNivLMD;
/**
 * Test class for the ClasseResource REST controller.
 *
 * @see ClasseResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApp.class)
public class ClasseResourceIntTest {

    private static final String DEFAULT_CODE_CLASSE = "AAAAAAAAAA";
    private static final String UPDATED_CODE_CLASSE = "BBBBBBBBBB";

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_TERMINAL = false;
    private static final Boolean UPDATED_TERMINAL = true;

    private static final EnumNivLMD DEFAULT_NIV_LMD = EnumNivLMD.L1;
    private static final EnumNivLMD UPDATED_NIV_LMD = EnumNivLMD.L2;

    private static final BigDecimal DEFAULT_FRAIS_INSCRIPTION = new BigDecimal(1);
    private static final BigDecimal UPDATED_FRAIS_INSCRIPTION = new BigDecimal(2);

    private static final Boolean DEFAULT_ENTRANT = false;
    private static final Boolean UPDATED_ENTRANT = true;

    private static final Boolean DEFAULT_SORTANT = false;
    private static final Boolean UPDATED_SORTANT = true;

    @Autowired
    private ClasseRepository classeRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restClasseMockMvc;

    private Classe classe;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClasseResource classeResource = new ClasseResource(classeRepository);
        this.restClasseMockMvc = MockMvcBuilders.standaloneSetup(classeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Classe createEntity(EntityManager em) {
        Classe classe = new Classe()
            .codeClasse(DEFAULT_CODE_CLASSE)
            .libelle(DEFAULT_LIBELLE)
            .terminal(DEFAULT_TERMINAL)
            .nivLMD(DEFAULT_NIV_LMD)
            .fraisInscription(DEFAULT_FRAIS_INSCRIPTION)
            .entrant(DEFAULT_ENTRANT)
            .sortant(DEFAULT_SORTANT);
        // Add required entity
        Formation formation = FormationResourceIntTest.createEntity(em);
        em.persist(formation);
        em.flush();
        classe.setFormation(formation);
        return classe;
    }

    @Before
    public void initTest() {
        classe = createEntity(em);
    }

    @Test
    @Transactional
    public void createClasse() throws Exception {
        int databaseSizeBeforeCreate = classeRepository.findAll().size();

        // Create the Classe
        restClasseMockMvc.perform(post("/api/classes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(classe)))
            .andExpect(status().isCreated());

        // Validate the Classe in the database
        List<Classe> classeList = classeRepository.findAll();
        assertThat(classeList).hasSize(databaseSizeBeforeCreate + 1);
        Classe testClasse = classeList.get(classeList.size() - 1);
        assertThat(testClasse.getCodeClasse()).isEqualTo(DEFAULT_CODE_CLASSE);
        assertThat(testClasse.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testClasse.isTerminal()).isEqualTo(DEFAULT_TERMINAL);
        assertThat(testClasse.getNivLMD()).isEqualTo(DEFAULT_NIV_LMD);
        assertThat(testClasse.getFraisInscription()).isEqualTo(DEFAULT_FRAIS_INSCRIPTION);
        assertThat(testClasse.isEntrant()).isEqualTo(DEFAULT_ENTRANT);
        assertThat(testClasse.isSortant()).isEqualTo(DEFAULT_SORTANT);
    }

    @Test
    @Transactional
    public void createClasseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = classeRepository.findAll().size();

        // Create the Classe with an existing ID
        classe.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClasseMockMvc.perform(post("/api/classes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(classe)))
            .andExpect(status().isBadRequest());

        // Validate the Classe in the database
        List<Classe> classeList = classeRepository.findAll();
        assertThat(classeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodeClasseIsRequired() throws Exception {
        int databaseSizeBeforeTest = classeRepository.findAll().size();
        // set the field null
        classe.setCodeClasse(null);

        // Create the Classe, which fails.

        restClasseMockMvc.perform(post("/api/classes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(classe)))
            .andExpect(status().isBadRequest());

        List<Classe> classeList = classeRepository.findAll();
        assertThat(classeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLibelleIsRequired() throws Exception {
        int databaseSizeBeforeTest = classeRepository.findAll().size();
        // set the field null
        classe.setLibelle(null);

        // Create the Classe, which fails.

        restClasseMockMvc.perform(post("/api/classes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(classe)))
            .andExpect(status().isBadRequest());

        List<Classe> classeList = classeRepository.findAll();
        assertThat(classeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTerminalIsRequired() throws Exception {
        int databaseSizeBeforeTest = classeRepository.findAll().size();
        // set the field null
        classe.setTerminal(null);

        // Create the Classe, which fails.

        restClasseMockMvc.perform(post("/api/classes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(classe)))
            .andExpect(status().isBadRequest());

        List<Classe> classeList = classeRepository.findAll();
        assertThat(classeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNivLMDIsRequired() throws Exception {
        int databaseSizeBeforeTest = classeRepository.findAll().size();
        // set the field null
        classe.setNivLMD(null);

        // Create the Classe, which fails.

        restClasseMockMvc.perform(post("/api/classes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(classe)))
            .andExpect(status().isBadRequest());

        List<Classe> classeList = classeRepository.findAll();
        assertThat(classeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFraisInscriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = classeRepository.findAll().size();
        // set the field null
        classe.setFraisInscription(null);

        // Create the Classe, which fails.

        restClasseMockMvc.perform(post("/api/classes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(classe)))
            .andExpect(status().isBadRequest());

        List<Classe> classeList = classeRepository.findAll();
        assertThat(classeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEntrantIsRequired() throws Exception {
        int databaseSizeBeforeTest = classeRepository.findAll().size();
        // set the field null
        classe.setEntrant(null);

        // Create the Classe, which fails.

        restClasseMockMvc.perform(post("/api/classes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(classe)))
            .andExpect(status().isBadRequest());

        List<Classe> classeList = classeRepository.findAll();
        assertThat(classeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSortantIsRequired() throws Exception {
        int databaseSizeBeforeTest = classeRepository.findAll().size();
        // set the field null
        classe.setSortant(null);

        // Create the Classe, which fails.

        restClasseMockMvc.perform(post("/api/classes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(classe)))
            .andExpect(status().isBadRequest());

        List<Classe> classeList = classeRepository.findAll();
        assertThat(classeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllClasses() throws Exception {
        // Initialize the database
        classeRepository.saveAndFlush(classe);

        // Get all the classeList
        restClasseMockMvc.perform(get("/api/classes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(classe.getId().intValue())))
            .andExpect(jsonPath("$.[*].codeClasse").value(hasItem(DEFAULT_CODE_CLASSE.toString())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE.toString())))
            .andExpect(jsonPath("$.[*].terminal").value(hasItem(DEFAULT_TERMINAL.booleanValue())))
            .andExpect(jsonPath("$.[*].nivLMD").value(hasItem(DEFAULT_NIV_LMD.toString())))
            .andExpect(jsonPath("$.[*].fraisInscription").value(hasItem(DEFAULT_FRAIS_INSCRIPTION.intValue())))
            .andExpect(jsonPath("$.[*].entrant").value(hasItem(DEFAULT_ENTRANT.booleanValue())))
            .andExpect(jsonPath("$.[*].sortant").value(hasItem(DEFAULT_SORTANT.booleanValue())));
    }

    @Test
    @Transactional
    public void getClasse() throws Exception {
        // Initialize the database
        classeRepository.saveAndFlush(classe);

        // Get the classe
        restClasseMockMvc.perform(get("/api/classes/{id}", classe.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(classe.getId().intValue()))
            .andExpect(jsonPath("$.codeClasse").value(DEFAULT_CODE_CLASSE.toString()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE.toString()))
            .andExpect(jsonPath("$.terminal").value(DEFAULT_TERMINAL.booleanValue()))
            .andExpect(jsonPath("$.nivLMD").value(DEFAULT_NIV_LMD.toString()))
            .andExpect(jsonPath("$.fraisInscription").value(DEFAULT_FRAIS_INSCRIPTION.intValue()))
            .andExpect(jsonPath("$.entrant").value(DEFAULT_ENTRANT.booleanValue()))
            .andExpect(jsonPath("$.sortant").value(DEFAULT_SORTANT.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingClasse() throws Exception {
        // Get the classe
        restClasseMockMvc.perform(get("/api/classes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClasse() throws Exception {
        // Initialize the database
        classeRepository.saveAndFlush(classe);
        int databaseSizeBeforeUpdate = classeRepository.findAll().size();

        // Update the classe
        Classe updatedClasse = classeRepository.findOne(classe.getId());
        // Disconnect from session so that the updates on updatedClasse are not directly saved in db
        em.detach(updatedClasse);
        updatedClasse
            .codeClasse(UPDATED_CODE_CLASSE)
            .libelle(UPDATED_LIBELLE)
            .terminal(UPDATED_TERMINAL)
            .nivLMD(UPDATED_NIV_LMD)
            .fraisInscription(UPDATED_FRAIS_INSCRIPTION)
            .entrant(UPDATED_ENTRANT)
            .sortant(UPDATED_SORTANT);

        restClasseMockMvc.perform(put("/api/classes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedClasse)))
            .andExpect(status().isOk());

        // Validate the Classe in the database
        List<Classe> classeList = classeRepository.findAll();
        assertThat(classeList).hasSize(databaseSizeBeforeUpdate);
        Classe testClasse = classeList.get(classeList.size() - 1);
        assertThat(testClasse.getCodeClasse()).isEqualTo(UPDATED_CODE_CLASSE);
        assertThat(testClasse.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testClasse.isTerminal()).isEqualTo(UPDATED_TERMINAL);
        assertThat(testClasse.getNivLMD()).isEqualTo(UPDATED_NIV_LMD);
        assertThat(testClasse.getFraisInscription()).isEqualTo(UPDATED_FRAIS_INSCRIPTION);
        assertThat(testClasse.isEntrant()).isEqualTo(UPDATED_ENTRANT);
        assertThat(testClasse.isSortant()).isEqualTo(UPDATED_SORTANT);
    }

    @Test
    @Transactional
    public void updateNonExistingClasse() throws Exception {
        int databaseSizeBeforeUpdate = classeRepository.findAll().size();

        // Create the Classe

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restClasseMockMvc.perform(put("/api/classes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(classe)))
            .andExpect(status().isCreated());

        // Validate the Classe in the database
        List<Classe> classeList = classeRepository.findAll();
        assertThat(classeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteClasse() throws Exception {
        // Initialize the database
        classeRepository.saveAndFlush(classe);
        int databaseSizeBeforeDelete = classeRepository.findAll().size();

        // Get the classe
        restClasseMockMvc.perform(delete("/api/classes/{id}", classe.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Classe> classeList = classeRepository.findAll();
        assertThat(classeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Classe.class);
        Classe classe1 = new Classe();
        classe1.setId(1L);
        Classe classe2 = new Classe();
        classe2.setId(classe1.getId());
        assertThat(classe1).isEqualTo(classe2);
        classe2.setId(2L);
        assertThat(classe1).isNotEqualTo(classe2);
        classe1.setId(null);
        assertThat(classe1).isNotEqualTo(classe2);
    }
}
