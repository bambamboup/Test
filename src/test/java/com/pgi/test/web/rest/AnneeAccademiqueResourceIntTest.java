package com.pgi.test.web.rest;

import com.pgi.test.TestApp;

import com.pgi.test.domain.AnneeAccademique;
import com.pgi.test.repository.AnneeAccademiqueRepository;
import com.pgi.test.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.pgi.test.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AnneeAccademiqueResource REST controller.
 *
 * @see AnneeAccademiqueResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApp.class)
public class AnneeAccademiqueResourceIntTest {

    private static final String DEFAULT_NOM_ANNEE_ACCADEMIQUE = "AAAAAAAAAA";
    private static final String UPDATED_NOM_ANNEE_ACCADEMIQUE = "BBBBBBBBBB";

    @Autowired
    private AnneeAccademiqueRepository anneeAccademiqueRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAnneeAccademiqueMockMvc;

    private AnneeAccademique anneeAccademique;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AnneeAccademiqueResource anneeAccademiqueResource = new AnneeAccademiqueResource(anneeAccademiqueRepository);
        this.restAnneeAccademiqueMockMvc = MockMvcBuilders.standaloneSetup(anneeAccademiqueResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AnneeAccademique createEntity(EntityManager em) {
        AnneeAccademique anneeAccademique = new AnneeAccademique()
            .nomAnneeAccademique(DEFAULT_NOM_ANNEE_ACCADEMIQUE);
        return anneeAccademique;
    }

    @Before
    public void initTest() {
        anneeAccademique = createEntity(em);
    }

    @Test
    @Transactional
    public void createAnneeAccademique() throws Exception {
        int databaseSizeBeforeCreate = anneeAccademiqueRepository.findAll().size();

        // Create the AnneeAccademique
        restAnneeAccademiqueMockMvc.perform(post("/api/annee-accademiques")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(anneeAccademique)))
            .andExpect(status().isCreated());

        // Validate the AnneeAccademique in the database
        List<AnneeAccademique> anneeAccademiqueList = anneeAccademiqueRepository.findAll();
        assertThat(anneeAccademiqueList).hasSize(databaseSizeBeforeCreate + 1);
        AnneeAccademique testAnneeAccademique = anneeAccademiqueList.get(anneeAccademiqueList.size() - 1);
        assertThat(testAnneeAccademique.getNomAnneeAccademique()).isEqualTo(DEFAULT_NOM_ANNEE_ACCADEMIQUE);
    }

    @Test
    @Transactional
    public void createAnneeAccademiqueWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = anneeAccademiqueRepository.findAll().size();

        // Create the AnneeAccademique with an existing ID
        anneeAccademique.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAnneeAccademiqueMockMvc.perform(post("/api/annee-accademiques")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(anneeAccademique)))
            .andExpect(status().isBadRequest());

        // Validate the AnneeAccademique in the database
        List<AnneeAccademique> anneeAccademiqueList = anneeAccademiqueRepository.findAll();
        assertThat(anneeAccademiqueList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNomAnneeAccademiqueIsRequired() throws Exception {
        int databaseSizeBeforeTest = anneeAccademiqueRepository.findAll().size();
        // set the field null
        anneeAccademique.setNomAnneeAccademique(null);

        // Create the AnneeAccademique, which fails.

        restAnneeAccademiqueMockMvc.perform(post("/api/annee-accademiques")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(anneeAccademique)))
            .andExpect(status().isBadRequest());

        List<AnneeAccademique> anneeAccademiqueList = anneeAccademiqueRepository.findAll();
        assertThat(anneeAccademiqueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAnneeAccademiques() throws Exception {
        // Initialize the database
        anneeAccademiqueRepository.saveAndFlush(anneeAccademique);

        // Get all the anneeAccademiqueList
        restAnneeAccademiqueMockMvc.perform(get("/api/annee-accademiques?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(anneeAccademique.getId().intValue())))
            .andExpect(jsonPath("$.[*].nomAnneeAccademique").value(hasItem(DEFAULT_NOM_ANNEE_ACCADEMIQUE.toString())));
    }

    @Test
    @Transactional
    public void getAnneeAccademique() throws Exception {
        // Initialize the database
        anneeAccademiqueRepository.saveAndFlush(anneeAccademique);

        // Get the anneeAccademique
        restAnneeAccademiqueMockMvc.perform(get("/api/annee-accademiques/{id}", anneeAccademique.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(anneeAccademique.getId().intValue()))
            .andExpect(jsonPath("$.nomAnneeAccademique").value(DEFAULT_NOM_ANNEE_ACCADEMIQUE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAnneeAccademique() throws Exception {
        // Get the anneeAccademique
        restAnneeAccademiqueMockMvc.perform(get("/api/annee-accademiques/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAnneeAccademique() throws Exception {
        // Initialize the database
        anneeAccademiqueRepository.saveAndFlush(anneeAccademique);
        int databaseSizeBeforeUpdate = anneeAccademiqueRepository.findAll().size();

        // Update the anneeAccademique
        AnneeAccademique updatedAnneeAccademique = anneeAccademiqueRepository.findOne(anneeAccademique.getId());
        // Disconnect from session so that the updates on updatedAnneeAccademique are not directly saved in db
        em.detach(updatedAnneeAccademique);
        updatedAnneeAccademique
            .nomAnneeAccademique(UPDATED_NOM_ANNEE_ACCADEMIQUE);

        restAnneeAccademiqueMockMvc.perform(put("/api/annee-accademiques")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAnneeAccademique)))
            .andExpect(status().isOk());

        // Validate the AnneeAccademique in the database
        List<AnneeAccademique> anneeAccademiqueList = anneeAccademiqueRepository.findAll();
        assertThat(anneeAccademiqueList).hasSize(databaseSizeBeforeUpdate);
        AnneeAccademique testAnneeAccademique = anneeAccademiqueList.get(anneeAccademiqueList.size() - 1);
        assertThat(testAnneeAccademique.getNomAnneeAccademique()).isEqualTo(UPDATED_NOM_ANNEE_ACCADEMIQUE);
    }

    @Test
    @Transactional
    public void updateNonExistingAnneeAccademique() throws Exception {
        int databaseSizeBeforeUpdate = anneeAccademiqueRepository.findAll().size();

        // Create the AnneeAccademique

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAnneeAccademiqueMockMvc.perform(put("/api/annee-accademiques")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(anneeAccademique)))
            .andExpect(status().isCreated());

        // Validate the AnneeAccademique in the database
        List<AnneeAccademique> anneeAccademiqueList = anneeAccademiqueRepository.findAll();
        assertThat(anneeAccademiqueList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAnneeAccademique() throws Exception {
        // Initialize the database
        anneeAccademiqueRepository.saveAndFlush(anneeAccademique);
        int databaseSizeBeforeDelete = anneeAccademiqueRepository.findAll().size();

        // Get the anneeAccademique
        restAnneeAccademiqueMockMvc.perform(delete("/api/annee-accademiques/{id}", anneeAccademique.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<AnneeAccademique> anneeAccademiqueList = anneeAccademiqueRepository.findAll();
        assertThat(anneeAccademiqueList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AnneeAccademique.class);
        AnneeAccademique anneeAccademique1 = new AnneeAccademique();
        anneeAccademique1.setId(1L);
        AnneeAccademique anneeAccademique2 = new AnneeAccademique();
        anneeAccademique2.setId(anneeAccademique1.getId());
        assertThat(anneeAccademique1).isEqualTo(anneeAccademique2);
        anneeAccademique2.setId(2L);
        assertThat(anneeAccademique1).isNotEqualTo(anneeAccademique2);
        anneeAccademique1.setId(null);
        assertThat(anneeAccademique1).isNotEqualTo(anneeAccademique2);
    }
}
