package com.pgi.test.web.rest;

import com.pgi.test.TestApp;

import com.pgi.test.domain.Journal;
import com.pgi.test.repository.JournalRepository;
import com.pgi.test.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.pgi.test.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the JournalResource REST controller.
 *
 * @see JournalResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApp.class)
public class JournalResourceIntTest {

    private static final String DEFAULT_CODE_JOURNAL = "AAAAAAAAAA";
    private static final String UPDATED_CODE_JOURNAL = "BBBBBBBBBB";

    private static final String DEFAULT_NOM_JOURNAL = "AAAAAAAAAA";
    private static final String UPDATED_NOM_JOURNAL = "BBBBBBBBBB";

    @Autowired
    private JournalRepository journalRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restJournalMockMvc;

    private Journal journal;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final JournalResource journalResource = new JournalResource(journalRepository);
        this.restJournalMockMvc = MockMvcBuilders.standaloneSetup(journalResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Journal createEntity(EntityManager em) {
        Journal journal = new Journal()
            .codeJournal(DEFAULT_CODE_JOURNAL)
            .nomJournal(DEFAULT_NOM_JOURNAL);
        return journal;
    }

    @Before
    public void initTest() {
        journal = createEntity(em);
    }

    @Test
    @Transactional
    public void createJournal() throws Exception {
        int databaseSizeBeforeCreate = journalRepository.findAll().size();

        // Create the Journal
        restJournalMockMvc.perform(post("/api/journals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(journal)))
            .andExpect(status().isCreated());

        // Validate the Journal in the database
        List<Journal> journalList = journalRepository.findAll();
        assertThat(journalList).hasSize(databaseSizeBeforeCreate + 1);
        Journal testJournal = journalList.get(journalList.size() - 1);
        assertThat(testJournal.getCodeJournal()).isEqualTo(DEFAULT_CODE_JOURNAL);
        assertThat(testJournal.getNomJournal()).isEqualTo(DEFAULT_NOM_JOURNAL);
    }

    @Test
    @Transactional
    public void createJournalWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = journalRepository.findAll().size();

        // Create the Journal with an existing ID
        journal.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJournalMockMvc.perform(post("/api/journals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(journal)))
            .andExpect(status().isBadRequest());

        // Validate the Journal in the database
        List<Journal> journalList = journalRepository.findAll();
        assertThat(journalList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNomJournalIsRequired() throws Exception {
        int databaseSizeBeforeTest = journalRepository.findAll().size();
        // set the field null
        journal.setNomJournal(null);

        // Create the Journal, which fails.

        restJournalMockMvc.perform(post("/api/journals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(journal)))
            .andExpect(status().isBadRequest());

        List<Journal> journalList = journalRepository.findAll();
        assertThat(journalList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllJournals() throws Exception {
        // Initialize the database
        journalRepository.saveAndFlush(journal);

        // Get all the journalList
        restJournalMockMvc.perform(get("/api/journals?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(journal.getId().intValue())))
            .andExpect(jsonPath("$.[*].codeJournal").value(hasItem(DEFAULT_CODE_JOURNAL.toString())))
            .andExpect(jsonPath("$.[*].nomJournal").value(hasItem(DEFAULT_NOM_JOURNAL.toString())));
    }

    @Test
    @Transactional
    public void getJournal() throws Exception {
        // Initialize the database
        journalRepository.saveAndFlush(journal);

        // Get the journal
        restJournalMockMvc.perform(get("/api/journals/{id}", journal.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(journal.getId().intValue()))
            .andExpect(jsonPath("$.codeJournal").value(DEFAULT_CODE_JOURNAL.toString()))
            .andExpect(jsonPath("$.nomJournal").value(DEFAULT_NOM_JOURNAL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingJournal() throws Exception {
        // Get the journal
        restJournalMockMvc.perform(get("/api/journals/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJournal() throws Exception {
        // Initialize the database
        journalRepository.saveAndFlush(journal);
        int databaseSizeBeforeUpdate = journalRepository.findAll().size();

        // Update the journal
        Journal updatedJournal = journalRepository.findOne(journal.getId());
        // Disconnect from session so that the updates on updatedJournal are not directly saved in db
        em.detach(updatedJournal);
        updatedJournal
            .codeJournal(UPDATED_CODE_JOURNAL)
            .nomJournal(UPDATED_NOM_JOURNAL);

        restJournalMockMvc.perform(put("/api/journals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedJournal)))
            .andExpect(status().isOk());

        // Validate the Journal in the database
        List<Journal> journalList = journalRepository.findAll();
        assertThat(journalList).hasSize(databaseSizeBeforeUpdate);
        Journal testJournal = journalList.get(journalList.size() - 1);
        assertThat(testJournal.getCodeJournal()).isEqualTo(UPDATED_CODE_JOURNAL);
        assertThat(testJournal.getNomJournal()).isEqualTo(UPDATED_NOM_JOURNAL);
    }

    @Test
    @Transactional
    public void updateNonExistingJournal() throws Exception {
        int databaseSizeBeforeUpdate = journalRepository.findAll().size();

        // Create the Journal

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restJournalMockMvc.perform(put("/api/journals")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(journal)))
            .andExpect(status().isCreated());

        // Validate the Journal in the database
        List<Journal> journalList = journalRepository.findAll();
        assertThat(journalList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteJournal() throws Exception {
        // Initialize the database
        journalRepository.saveAndFlush(journal);
        int databaseSizeBeforeDelete = journalRepository.findAll().size();

        // Get the journal
        restJournalMockMvc.perform(delete("/api/journals/{id}", journal.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Journal> journalList = journalRepository.findAll();
        assertThat(journalList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Journal.class);
        Journal journal1 = new Journal();
        journal1.setId(1L);
        Journal journal2 = new Journal();
        journal2.setId(journal1.getId());
        assertThat(journal1).isEqualTo(journal2);
        journal2.setId(2L);
        assertThat(journal1).isNotEqualTo(journal2);
        journal1.setId(null);
        assertThat(journal1).isNotEqualTo(journal2);
    }
}
