package com.pgi.test.web.rest;

import com.pgi.test.TestApp;

import com.pgi.test.domain.TypeFormation;
import com.pgi.test.repository.TypeFormationRepository;
import com.pgi.test.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.pgi.test.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.pgi.test.domain.enumeration.EnumTypeFormation;
/**
 * Test class for the TypeFormationResource REST controller.
 *
 * @see TypeFormationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApp.class)
public class TypeFormationResourceIntTest {

    private static final EnumTypeFormation DEFAULT_TYPE_FORMAT = EnumTypeFormation.FPCT;
    private static final EnumTypeFormation UPDATED_TYPE_FORMAT = EnumTypeFormation.FPCE;

    @Autowired
    private TypeFormationRepository typeFormationRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTypeFormationMockMvc;

    private TypeFormation typeFormation;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TypeFormationResource typeFormationResource = new TypeFormationResource(typeFormationRepository);
        this.restTypeFormationMockMvc = MockMvcBuilders.standaloneSetup(typeFormationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TypeFormation createEntity(EntityManager em) {
        TypeFormation typeFormation = new TypeFormation()
            .typeFormat(DEFAULT_TYPE_FORMAT);
        return typeFormation;
    }

    @Before
    public void initTest() {
        typeFormation = createEntity(em);
    }

    @Test
    @Transactional
    public void createTypeFormation() throws Exception {
        int databaseSizeBeforeCreate = typeFormationRepository.findAll().size();

        // Create the TypeFormation
        restTypeFormationMockMvc.perform(post("/api/type-formations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(typeFormation)))
            .andExpect(status().isCreated());

        // Validate the TypeFormation in the database
        List<TypeFormation> typeFormationList = typeFormationRepository.findAll();
        assertThat(typeFormationList).hasSize(databaseSizeBeforeCreate + 1);
        TypeFormation testTypeFormation = typeFormationList.get(typeFormationList.size() - 1);
        assertThat(testTypeFormation.getTypeFormat()).isEqualTo(DEFAULT_TYPE_FORMAT);
    }

    @Test
    @Transactional
    public void createTypeFormationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = typeFormationRepository.findAll().size();

        // Create the TypeFormation with an existing ID
        typeFormation.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTypeFormationMockMvc.perform(post("/api/type-formations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(typeFormation)))
            .andExpect(status().isBadRequest());

        // Validate the TypeFormation in the database
        List<TypeFormation> typeFormationList = typeFormationRepository.findAll();
        assertThat(typeFormationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTypeFormatIsRequired() throws Exception {
        int databaseSizeBeforeTest = typeFormationRepository.findAll().size();
        // set the field null
        typeFormation.setTypeFormat(null);

        // Create the TypeFormation, which fails.

        restTypeFormationMockMvc.perform(post("/api/type-formations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(typeFormation)))
            .andExpect(status().isBadRequest());

        List<TypeFormation> typeFormationList = typeFormationRepository.findAll();
        assertThat(typeFormationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTypeFormations() throws Exception {
        // Initialize the database
        typeFormationRepository.saveAndFlush(typeFormation);

        // Get all the typeFormationList
        restTypeFormationMockMvc.perform(get("/api/type-formations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(typeFormation.getId().intValue())))
            .andExpect(jsonPath("$.[*].typeFormat").value(hasItem(DEFAULT_TYPE_FORMAT.toString())));
    }

    @Test
    @Transactional
    public void getTypeFormation() throws Exception {
        // Initialize the database
        typeFormationRepository.saveAndFlush(typeFormation);

        // Get the typeFormation
        restTypeFormationMockMvc.perform(get("/api/type-formations/{id}", typeFormation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(typeFormation.getId().intValue()))
            .andExpect(jsonPath("$.typeFormat").value(DEFAULT_TYPE_FORMAT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTypeFormation() throws Exception {
        // Get the typeFormation
        restTypeFormationMockMvc.perform(get("/api/type-formations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTypeFormation() throws Exception {
        // Initialize the database
        typeFormationRepository.saveAndFlush(typeFormation);
        int databaseSizeBeforeUpdate = typeFormationRepository.findAll().size();

        // Update the typeFormation
        TypeFormation updatedTypeFormation = typeFormationRepository.findOne(typeFormation.getId());
        // Disconnect from session so that the updates on updatedTypeFormation are not directly saved in db
        em.detach(updatedTypeFormation);
        updatedTypeFormation
            .typeFormat(UPDATED_TYPE_FORMAT);

        restTypeFormationMockMvc.perform(put("/api/type-formations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTypeFormation)))
            .andExpect(status().isOk());

        // Validate the TypeFormation in the database
        List<TypeFormation> typeFormationList = typeFormationRepository.findAll();
        assertThat(typeFormationList).hasSize(databaseSizeBeforeUpdate);
        TypeFormation testTypeFormation = typeFormationList.get(typeFormationList.size() - 1);
        assertThat(testTypeFormation.getTypeFormat()).isEqualTo(UPDATED_TYPE_FORMAT);
    }

    @Test
    @Transactional
    public void updateNonExistingTypeFormation() throws Exception {
        int databaseSizeBeforeUpdate = typeFormationRepository.findAll().size();

        // Create the TypeFormation

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTypeFormationMockMvc.perform(put("/api/type-formations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(typeFormation)))
            .andExpect(status().isCreated());

        // Validate the TypeFormation in the database
        List<TypeFormation> typeFormationList = typeFormationRepository.findAll();
        assertThat(typeFormationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTypeFormation() throws Exception {
        // Initialize the database
        typeFormationRepository.saveAndFlush(typeFormation);
        int databaseSizeBeforeDelete = typeFormationRepository.findAll().size();

        // Get the typeFormation
        restTypeFormationMockMvc.perform(delete("/api/type-formations/{id}", typeFormation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TypeFormation> typeFormationList = typeFormationRepository.findAll();
        assertThat(typeFormationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TypeFormation.class);
        TypeFormation typeFormation1 = new TypeFormation();
        typeFormation1.setId(1L);
        TypeFormation typeFormation2 = new TypeFormation();
        typeFormation2.setId(typeFormation1.getId());
        assertThat(typeFormation1).isEqualTo(typeFormation2);
        typeFormation2.setId(2L);
        assertThat(typeFormation1).isNotEqualTo(typeFormation2);
        typeFormation1.setId(null);
        assertThat(typeFormation1).isNotEqualTo(typeFormation2);
    }
}
