package com.pgi.test.web.rest;

import com.pgi.test.TestApp;

import com.pgi.test.domain.TiersPayant;
import com.pgi.test.repository.TiersPayantRepository;
import com.pgi.test.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.pgi.test.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TiersPayantResource REST controller.
 *
 * @see TiersPayantResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApp.class)
public class TiersPayantResourceIntTest {

    @Autowired
    private TiersPayantRepository tiersPayantRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTiersPayantMockMvc;

    private TiersPayant tiersPayant;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TiersPayantResource tiersPayantResource = new TiersPayantResource(tiersPayantRepository);
        this.restTiersPayantMockMvc = MockMvcBuilders.standaloneSetup(tiersPayantResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TiersPayant createEntity(EntityManager em) {
        TiersPayant tiersPayant = new TiersPayant();
        return tiersPayant;
    }

    @Before
    public void initTest() {
        tiersPayant = createEntity(em);
    }

    @Test
    @Transactional
    public void createTiersPayant() throws Exception {
        int databaseSizeBeforeCreate = tiersPayantRepository.findAll().size();

        // Create the TiersPayant
        restTiersPayantMockMvc.perform(post("/api/tiers-payants")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tiersPayant)))
            .andExpect(status().isCreated());

        // Validate the TiersPayant in the database
        List<TiersPayant> tiersPayantList = tiersPayantRepository.findAll();
        assertThat(tiersPayantList).hasSize(databaseSizeBeforeCreate + 1);
        TiersPayant testTiersPayant = tiersPayantList.get(tiersPayantList.size() - 1);
    }

    @Test
    @Transactional
    public void createTiersPayantWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tiersPayantRepository.findAll().size();

        // Create the TiersPayant with an existing ID
        tiersPayant.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTiersPayantMockMvc.perform(post("/api/tiers-payants")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tiersPayant)))
            .andExpect(status().isBadRequest());

        // Validate the TiersPayant in the database
        List<TiersPayant> tiersPayantList = tiersPayantRepository.findAll();
        assertThat(tiersPayantList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTiersPayants() throws Exception {
        // Initialize the database
        tiersPayantRepository.saveAndFlush(tiersPayant);

        // Get all the tiersPayantList
        restTiersPayantMockMvc.perform(get("/api/tiers-payants?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tiersPayant.getId().intValue())));
    }

    @Test
    @Transactional
    public void getTiersPayant() throws Exception {
        // Initialize the database
        tiersPayantRepository.saveAndFlush(tiersPayant);

        // Get the tiersPayant
        restTiersPayantMockMvc.perform(get("/api/tiers-payants/{id}", tiersPayant.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tiersPayant.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTiersPayant() throws Exception {
        // Get the tiersPayant
        restTiersPayantMockMvc.perform(get("/api/tiers-payants/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTiersPayant() throws Exception {
        // Initialize the database
        tiersPayantRepository.saveAndFlush(tiersPayant);
        int databaseSizeBeforeUpdate = tiersPayantRepository.findAll().size();

        // Update the tiersPayant
        TiersPayant updatedTiersPayant = tiersPayantRepository.findOne(tiersPayant.getId());
        // Disconnect from session so that the updates on updatedTiersPayant are not directly saved in db
        em.detach(updatedTiersPayant);

        restTiersPayantMockMvc.perform(put("/api/tiers-payants")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTiersPayant)))
            .andExpect(status().isOk());

        // Validate the TiersPayant in the database
        List<TiersPayant> tiersPayantList = tiersPayantRepository.findAll();
        assertThat(tiersPayantList).hasSize(databaseSizeBeforeUpdate);
        TiersPayant testTiersPayant = tiersPayantList.get(tiersPayantList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingTiersPayant() throws Exception {
        int databaseSizeBeforeUpdate = tiersPayantRepository.findAll().size();

        // Create the TiersPayant

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTiersPayantMockMvc.perform(put("/api/tiers-payants")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tiersPayant)))
            .andExpect(status().isCreated());

        // Validate the TiersPayant in the database
        List<TiersPayant> tiersPayantList = tiersPayantRepository.findAll();
        assertThat(tiersPayantList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTiersPayant() throws Exception {
        // Initialize the database
        tiersPayantRepository.saveAndFlush(tiersPayant);
        int databaseSizeBeforeDelete = tiersPayantRepository.findAll().size();

        // Get the tiersPayant
        restTiersPayantMockMvc.perform(delete("/api/tiers-payants/{id}", tiersPayant.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TiersPayant> tiersPayantList = tiersPayantRepository.findAll();
        assertThat(tiersPayantList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TiersPayant.class);
        TiersPayant tiersPayant1 = new TiersPayant();
        tiersPayant1.setId(1L);
        TiersPayant tiersPayant2 = new TiersPayant();
        tiersPayant2.setId(tiersPayant1.getId());
        assertThat(tiersPayant1).isEqualTo(tiersPayant2);
        tiersPayant2.setId(2L);
        assertThat(tiersPayant1).isNotEqualTo(tiersPayant2);
        tiersPayant1.setId(null);
        assertThat(tiersPayant1).isNotEqualTo(tiersPayant2);
    }
}
