/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestTestModule } from '../../../test.module';
import { TypeEvaluationComponent } from '../../../../../../main/webapp/app/entities/type-evaluation/type-evaluation.component';
import { TypeEvaluationService } from '../../../../../../main/webapp/app/entities/type-evaluation/type-evaluation.service';
import { TypeEvaluation } from '../../../../../../main/webapp/app/entities/type-evaluation/type-evaluation.model';

describe('Component Tests', () => {

    describe('TypeEvaluation Management Component', () => {
        let comp: TypeEvaluationComponent;
        let fixture: ComponentFixture<TypeEvaluationComponent>;
        let service: TypeEvaluationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [TypeEvaluationComponent],
                providers: [
                    TypeEvaluationService
                ]
            })
            .overrideTemplate(TypeEvaluationComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TypeEvaluationComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TypeEvaluationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new TypeEvaluation(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.typeEvaluations[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
