/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestTestModule } from '../../../test.module';
import { TypeEvaluationDetailComponent } from '../../../../../../main/webapp/app/entities/type-evaluation/type-evaluation-detail.component';
import { TypeEvaluationService } from '../../../../../../main/webapp/app/entities/type-evaluation/type-evaluation.service';
import { TypeEvaluation } from '../../../../../../main/webapp/app/entities/type-evaluation/type-evaluation.model';

describe('Component Tests', () => {

    describe('TypeEvaluation Management Detail Component', () => {
        let comp: TypeEvaluationDetailComponent;
        let fixture: ComponentFixture<TypeEvaluationDetailComponent>;
        let service: TypeEvaluationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [TypeEvaluationDetailComponent],
                providers: [
                    TypeEvaluationService
                ]
            })
            .overrideTemplate(TypeEvaluationDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TypeEvaluationDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TypeEvaluationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new TypeEvaluation(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.typeEvaluation).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
