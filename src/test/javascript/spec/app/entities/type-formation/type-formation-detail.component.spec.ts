/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestTestModule } from '../../../test.module';
import { TypeFormationDetailComponent } from '../../../../../../main/webapp/app/entities/type-formation/type-formation-detail.component';
import { TypeFormationService } from '../../../../../../main/webapp/app/entities/type-formation/type-formation.service';
import { TypeFormation } from '../../../../../../main/webapp/app/entities/type-formation/type-formation.model';

describe('Component Tests', () => {

    describe('TypeFormation Management Detail Component', () => {
        let comp: TypeFormationDetailComponent;
        let fixture: ComponentFixture<TypeFormationDetailComponent>;
        let service: TypeFormationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [TypeFormationDetailComponent],
                providers: [
                    TypeFormationService
                ]
            })
            .overrideTemplate(TypeFormationDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TypeFormationDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TypeFormationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new TypeFormation(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.typeFormation).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
