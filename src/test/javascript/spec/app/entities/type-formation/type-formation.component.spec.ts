/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestTestModule } from '../../../test.module';
import { TypeFormationComponent } from '../../../../../../main/webapp/app/entities/type-formation/type-formation.component';
import { TypeFormationService } from '../../../../../../main/webapp/app/entities/type-formation/type-formation.service';
import { TypeFormation } from '../../../../../../main/webapp/app/entities/type-formation/type-formation.model';

describe('Component Tests', () => {

    describe('TypeFormation Management Component', () => {
        let comp: TypeFormationComponent;
        let fixture: ComponentFixture<TypeFormationComponent>;
        let service: TypeFormationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [TypeFormationComponent],
                providers: [
                    TypeFormationService
                ]
            })
            .overrideTemplate(TypeFormationComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TypeFormationComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TypeFormationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new TypeFormation(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.typeFormations[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
