/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TestTestModule } from '../../../test.module';
import { FormationDialogComponent } from '../../../../../../main/webapp/app/entities/formation/formation-dialog.component';
import { FormationService } from '../../../../../../main/webapp/app/entities/formation/formation.service';
import { Formation } from '../../../../../../main/webapp/app/entities/formation/formation.model';
import { StructureService } from '../../../../../../main/webapp/app/entities/structure';
import { MembreService } from '../../../../../../main/webapp/app/entities/membre';
import { TypeFormationService } from '../../../../../../main/webapp/app/entities/type-formation';
import { TypeEvaluationService } from '../../../../../../main/webapp/app/entities/type-evaluation';

describe('Component Tests', () => {

    describe('Formation Management Dialog Component', () => {
        let comp: FormationDialogComponent;
        let fixture: ComponentFixture<FormationDialogComponent>;
        let service: FormationService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [FormationDialogComponent],
                providers: [
                    StructureService,
                    MembreService,
                    TypeFormationService,
                    TypeEvaluationService,
                    FormationService
                ]
            })
            .overrideTemplate(FormationDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(FormationDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(FormationService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Formation(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.formation = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'formationListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Formation();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.formation = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'formationListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
