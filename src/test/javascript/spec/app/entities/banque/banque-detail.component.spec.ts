/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestTestModule } from '../../../test.module';
import { BanqueDetailComponent } from '../../../../../../main/webapp/app/entities/banque/banque-detail.component';
import { BanqueService } from '../../../../../../main/webapp/app/entities/banque/banque.service';
import { Banque } from '../../../../../../main/webapp/app/entities/banque/banque.model';

describe('Component Tests', () => {

    describe('Banque Management Detail Component', () => {
        let comp: BanqueDetailComponent;
        let fixture: ComponentFixture<BanqueDetailComponent>;
        let service: BanqueService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [BanqueDetailComponent],
                providers: [
                    BanqueService
                ]
            })
            .overrideTemplate(BanqueDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BanqueDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BanqueService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Banque(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.banque).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
