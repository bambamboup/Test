/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestTestModule } from '../../../test.module';
import { BanqueComponent } from '../../../../../../main/webapp/app/entities/banque/banque.component';
import { BanqueService } from '../../../../../../main/webapp/app/entities/banque/banque.service';
import { Banque } from '../../../../../../main/webapp/app/entities/banque/banque.model';

describe('Component Tests', () => {

    describe('Banque Management Component', () => {
        let comp: BanqueComponent;
        let fixture: ComponentFixture<BanqueComponent>;
        let service: BanqueService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [BanqueComponent],
                providers: [
                    BanqueService
                ]
            })
            .overrideTemplate(BanqueComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BanqueComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BanqueService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Banque(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.banques[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
