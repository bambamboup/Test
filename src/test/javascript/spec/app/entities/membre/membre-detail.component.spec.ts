/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestTestModule } from '../../../test.module';
import { MembreDetailComponent } from '../../../../../../main/webapp/app/entities/membre/membre-detail.component';
import { MembreService } from '../../../../../../main/webapp/app/entities/membre/membre.service';
import { Membre } from '../../../../../../main/webapp/app/entities/membre/membre.model';

describe('Component Tests', () => {

    describe('Membre Management Detail Component', () => {
        let comp: MembreDetailComponent;
        let fixture: ComponentFixture<MembreDetailComponent>;
        let service: MembreService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [MembreDetailComponent],
                providers: [
                    MembreService
                ]
            })
            .overrideTemplate(MembreDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MembreDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MembreService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Membre(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.membre).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
