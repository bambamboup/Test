/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestTestModule } from '../../../test.module';
import { MembreComponent } from '../../../../../../main/webapp/app/entities/membre/membre.component';
import { MembreService } from '../../../../../../main/webapp/app/entities/membre/membre.service';
import { Membre } from '../../../../../../main/webapp/app/entities/membre/membre.model';

describe('Component Tests', () => {

    describe('Membre Management Component', () => {
        let comp: MembreComponent;
        let fixture: ComponentFixture<MembreComponent>;
        let service: MembreService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [MembreComponent],
                providers: [
                    MembreService
                ]
            })
            .overrideTemplate(MembreComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MembreComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MembreService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Membre(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.membres[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
