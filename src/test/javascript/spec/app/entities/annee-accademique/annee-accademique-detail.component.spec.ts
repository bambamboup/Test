/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestTestModule } from '../../../test.module';
import { AnneeAccademiqueDetailComponent } from '../../../../../../main/webapp/app/entities/annee-accademique/annee-accademique-detail.component';
import { AnneeAccademiqueService } from '../../../../../../main/webapp/app/entities/annee-accademique/annee-accademique.service';
import { AnneeAccademique } from '../../../../../../main/webapp/app/entities/annee-accademique/annee-accademique.model';

describe('Component Tests', () => {

    describe('AnneeAccademique Management Detail Component', () => {
        let comp: AnneeAccademiqueDetailComponent;
        let fixture: ComponentFixture<AnneeAccademiqueDetailComponent>;
        let service: AnneeAccademiqueService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [AnneeAccademiqueDetailComponent],
                providers: [
                    AnneeAccademiqueService
                ]
            })
            .overrideTemplate(AnneeAccademiqueDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AnneeAccademiqueDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AnneeAccademiqueService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new AnneeAccademique(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.anneeAccademique).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
