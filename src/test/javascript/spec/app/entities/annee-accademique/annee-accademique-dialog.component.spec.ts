/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TestTestModule } from '../../../test.module';
import { AnneeAccademiqueDialogComponent } from '../../../../../../main/webapp/app/entities/annee-accademique/annee-accademique-dialog.component';
import { AnneeAccademiqueService } from '../../../../../../main/webapp/app/entities/annee-accademique/annee-accademique.service';
import { AnneeAccademique } from '../../../../../../main/webapp/app/entities/annee-accademique/annee-accademique.model';

describe('Component Tests', () => {

    describe('AnneeAccademique Management Dialog Component', () => {
        let comp: AnneeAccademiqueDialogComponent;
        let fixture: ComponentFixture<AnneeAccademiqueDialogComponent>;
        let service: AnneeAccademiqueService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [AnneeAccademiqueDialogComponent],
                providers: [
                    AnneeAccademiqueService
                ]
            })
            .overrideTemplate(AnneeAccademiqueDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AnneeAccademiqueDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AnneeAccademiqueService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new AnneeAccademique(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.anneeAccademique = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'anneeAccademiqueListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new AnneeAccademique();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.anneeAccademique = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'anneeAccademiqueListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
