/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestTestModule } from '../../../test.module';
import { AnneeAccademiqueComponent } from '../../../../../../main/webapp/app/entities/annee-accademique/annee-accademique.component';
import { AnneeAccademiqueService } from '../../../../../../main/webapp/app/entities/annee-accademique/annee-accademique.service';
import { AnneeAccademique } from '../../../../../../main/webapp/app/entities/annee-accademique/annee-accademique.model';

describe('Component Tests', () => {

    describe('AnneeAccademique Management Component', () => {
        let comp: AnneeAccademiqueComponent;
        let fixture: ComponentFixture<AnneeAccademiqueComponent>;
        let service: AnneeAccademiqueService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [AnneeAccademiqueComponent],
                providers: [
                    AnneeAccademiqueService
                ]
            })
            .overrideTemplate(AnneeAccademiqueComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AnneeAccademiqueComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AnneeAccademiqueService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new AnneeAccademique(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.anneeAccademiques[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
