/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TestTestModule } from '../../../test.module';
import { AnneeAccademiqueDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/annee-accademique/annee-accademique-delete-dialog.component';
import { AnneeAccademiqueService } from '../../../../../../main/webapp/app/entities/annee-accademique/annee-accademique.service';

describe('Component Tests', () => {

    describe('AnneeAccademique Management Delete Component', () => {
        let comp: AnneeAccademiqueDeleteDialogComponent;
        let fixture: ComponentFixture<AnneeAccademiqueDeleteDialogComponent>;
        let service: AnneeAccademiqueService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [AnneeAccademiqueDeleteDialogComponent],
                providers: [
                    AnneeAccademiqueService
                ]
            })
            .overrideTemplate(AnneeAccademiqueDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AnneeAccademiqueDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AnneeAccademiqueService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
