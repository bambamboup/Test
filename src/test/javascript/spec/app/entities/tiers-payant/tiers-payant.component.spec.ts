/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestTestModule } from '../../../test.module';
import { TiersPayantComponent } from '../../../../../../main/webapp/app/entities/tiers-payant/tiers-payant.component';
import { TiersPayantService } from '../../../../../../main/webapp/app/entities/tiers-payant/tiers-payant.service';
import { TiersPayant } from '../../../../../../main/webapp/app/entities/tiers-payant/tiers-payant.model';

describe('Component Tests', () => {

    describe('TiersPayant Management Component', () => {
        let comp: TiersPayantComponent;
        let fixture: ComponentFixture<TiersPayantComponent>;
        let service: TiersPayantService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestTestModule],
                declarations: [TiersPayantComponent],
                providers: [
                    TiersPayantService
                ]
            })
            .overrideTemplate(TiersPayantComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TiersPayantComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TiersPayantService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new TiersPayant(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.tiersPayants[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
