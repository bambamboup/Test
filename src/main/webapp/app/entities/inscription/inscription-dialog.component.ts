import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Inscription } from './inscription.model';
import { InscriptionPopupService } from './inscription-popup.service';
import { InscriptionService } from './inscription.service';
import { Classe, ClasseService } from '../classe';
import { Etudiant, EtudiantService } from '../etudiant';
import { AnneeAccademique, AnneeAccademiqueService } from '../annee-accademique';

@Component({
    selector: 'jhi-inscription-dialog',
    templateUrl: './inscription-dialog.component.html'
})
export class InscriptionDialogComponent implements OnInit {

    inscription: Inscription;
    isSaving: boolean;

    classes: Classe[];

    etudiants: Etudiant[];

    anneeaccademiques: AnneeAccademique[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private inscriptionService: InscriptionService,
        private classeService: ClasseService,
        private etudiantService: EtudiantService,
        private anneeAccademiqueService: AnneeAccademiqueService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.classeService.query()
            .subscribe((res: HttpResponse<Classe[]>) => { this.classes = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.etudiantService.query()
            .subscribe((res: HttpResponse<Etudiant[]>) => { this.etudiants = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.anneeAccademiqueService.query()
            .subscribe((res: HttpResponse<AnneeAccademique[]>) => { this.anneeaccademiques = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.inscription.id !== undefined) {
            this.subscribeToSaveResponse(
                this.inscriptionService.update(this.inscription));
        } else {
            this.subscribeToSaveResponse(
                this.inscriptionService.create(this.inscription));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Inscription>>) {
        result.subscribe((res: HttpResponse<Inscription>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Inscription) {
        this.eventManager.broadcast({ name: 'inscriptionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackClasseById(index: number, item: Classe) {
        return item.id;
    }

    trackEtudiantById(index: number, item: Etudiant) {
        return item.id;
    }

    trackAnneeAccademiqueById(index: number, item: AnneeAccademique) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-inscription-popup',
    template: ''
})
export class InscriptionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private inscriptionPopupService: InscriptionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.inscriptionPopupService
                    .open(InscriptionDialogComponent as Component, params['id']);
            } else {
                this.inscriptionPopupService
                    .open(InscriptionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
