import { BaseEntity } from './../../shared';

export const enum EnumEtat {
    'ENCOURS',
    'ABANDON'
}

export class Inscription implements BaseEntity {
    constructor(
        public id?: number,
        public etat?: EnumEtat,
        public classe?: BaseEntity,
        public etudiant?: BaseEntity,
        public anneeAccademique?: BaseEntity,
    ) {
    }
}
