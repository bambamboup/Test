import { BaseEntity } from './../../shared';

export class Nationalite implements BaseEntity {
    constructor(
        public id?: number,
        public nomNationalite?: string,
        public etudiants?: BaseEntity[],
    ) {
    }
}
