import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { NationaliteComponent } from './nationalite.component';
import { NationaliteDetailComponent } from './nationalite-detail.component';
import { NationalitePopupComponent } from './nationalite-dialog.component';
import { NationaliteDeletePopupComponent } from './nationalite-delete-dialog.component';

export const nationaliteRoute: Routes = [
    {
        path: 'nationalite',
        component: NationaliteComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.nationalite.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'nationalite/:id',
        component: NationaliteDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.nationalite.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const nationalitePopupRoute: Routes = [
    {
        path: 'nationalite-new',
        component: NationalitePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.nationalite.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'nationalite/:id/edit',
        component: NationalitePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.nationalite.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'nationalite/:id/delete',
        component: NationaliteDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.nationalite.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
