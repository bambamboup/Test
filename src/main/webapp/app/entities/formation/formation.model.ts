import { BaseEntity } from './../../shared';

export class Formation implements BaseEntity {
    constructor(
        public id?: number,
        public codeFormation?: string,
        public nomFormation?: string,
        public mensualite?: number,
        public classes?: BaseEntity[],
        public departement?: BaseEntity,
        public responsable?: BaseEntity,
        public typeFormation?: BaseEntity,
        public typeEvaluatiion?: BaseEntity,
    ) {
    }
}
