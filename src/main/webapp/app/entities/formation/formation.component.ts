import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Formation } from './formation.model';
import { FormationService } from './formation.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-formation',
    templateUrl: './formation.component.html'
})
export class FormationComponent implements OnInit, OnDestroy {
formations: Formation[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private formationService: FormationService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.formationService.query().subscribe(
            (res: HttpResponse<Formation[]>) => {
                this.formations = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInFormations();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Formation) {
        return item.id;
    }
    registerChangeInFormations() {
        this.eventSubscriber = this.eventManager.subscribe('formationListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
