import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Formation } from './formation.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Formation>;

@Injectable()
export class FormationService {

    private resourceUrl =  SERVER_API_URL + 'api/formations';

    constructor(private http: HttpClient) { }

    create(formation: Formation): Observable<EntityResponseType> {
        const copy = this.convert(formation);
        return this.http.post<Formation>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(formation: Formation): Observable<EntityResponseType> {
        const copy = this.convert(formation);
        return this.http.put<Formation>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Formation>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Formation[]>> {
        const options = createRequestOption(req);
        return this.http.get<Formation[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Formation[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Formation = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Formation[]>): HttpResponse<Formation[]> {
        const jsonResponse: Formation[] = res.body;
        const body: Formation[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Formation.
     */
    private convertItemFromServer(formation: Formation): Formation {
        const copy: Formation = Object.assign({}, formation);
        return copy;
    }

    /**
     * Convert a Formation to a JSON which can be sent to the server.
     */
    private convert(formation: Formation): Formation {
        const copy: Formation = Object.assign({}, formation);
        return copy;
    }
}
