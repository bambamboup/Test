import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Formation } from './formation.model';
import { FormationPopupService } from './formation-popup.service';
import { FormationService } from './formation.service';
import { Structure, StructureService } from '../structure';
import { Membre, MembreService } from '../membre';
import { TypeFormation, TypeFormationService } from '../type-formation';
import { TypeEvaluation, TypeEvaluationService } from '../type-evaluation';

@Component({
    selector: 'jhi-formation-dialog',
    templateUrl: './formation-dialog.component.html'
})
export class FormationDialogComponent implements OnInit {

    formation: Formation;
    isSaving: boolean;

    structures: Structure[];

    membres: Membre[];

    typeformations: TypeFormation[];

    typeevaluations: TypeEvaluation[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private formationService: FormationService,
        private structureService: StructureService,
        private membreService: MembreService,
        private typeFormationService: TypeFormationService,
        private typeEvaluationService: TypeEvaluationService,
        private eventManager: JhiEventManager,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.structureService.query()
            .subscribe((res: HttpResponse<Structure[]>) => { this.structures = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.membreService.query()
            .subscribe((res: HttpResponse<Membre[]>) => { this.membres = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.typeFormationService.query()
            .subscribe((res: HttpResponse<TypeFormation[]>) => { this.typeformations = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.typeEvaluationService.query()
            .subscribe((res: HttpResponse<TypeEvaluation[]>) => { this.typeevaluations = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.formation.id !== undefined) {
            this.subscribeToSaveResponse(
                this.formationService.update(this.formation));
        } else {
            this.subscribeToSaveResponse(
                this.formationService.create(this.formation));
        }
    }

    ajouterStructure() {
        this.router.navigate(['structure-new']);
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Formation>>) {
        result.subscribe((res: HttpResponse<Formation>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Formation) {
        this.eventManager.broadcast({ name: 'formationListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackStructureById(index: number, item: Structure) {
        return item.id;
    }

    trackMembreById(index: number, item: Membre) {
        return item.id;
    }

    trackTypeFormationById(index: number, item: TypeFormation) {
        return item.id;
    }

    trackTypeEvaluationById(index: number, item: TypeEvaluation) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-formation-popup',
    template: ''
})
export class FormationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private formationPopupService: FormationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.formationPopupService
                    .open(FormationDialogComponent as Component, params['id']);
            } else {
                this.formationPopupService
                    .open(FormationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
