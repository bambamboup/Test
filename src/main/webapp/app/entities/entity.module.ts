import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { TestBanqueModule } from './banque/banque.module';
import { TestFournisseurModule } from './fournisseur/fournisseur.module';
import { TestTypeFormationModule } from './type-formation/type-formation.module';
import { TestTypeEvaluationModule } from './type-evaluation/type-evaluation.module';
import { TestStructureModule } from './structure/structure.module';
import { TestJournalModule } from './journal/journal.module';
import { TestMembreModule } from './membre/membre.module';
import { TestFormationModule } from './formation/formation.module';
import { TestClasseModule } from './classe/classe.module';
import { TestNationaliteModule } from './nationalite/nationalite.module';
import { TestTiersPayantModule } from './tiers-payant/tiers-payant.module';
import { TestEtudiantModule } from './etudiant/etudiant.module';
import { TestAnneeAccademiqueModule } from './annee-accademique/annee-accademique.module';
import { TestInscriptionModule } from './inscription/inscription.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        TestBanqueModule,
        TestFournisseurModule,
        TestTypeFormationModule,
        TestTypeEvaluationModule,
        TestStructureModule,
        TestJournalModule,
        TestMembreModule,
        TestFormationModule,
        TestClasseModule,
        TestNationaliteModule,
        TestTiersPayantModule,
        TestEtudiantModule,
        TestAnneeAccademiqueModule,
        TestInscriptionModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestEntityModule {}
