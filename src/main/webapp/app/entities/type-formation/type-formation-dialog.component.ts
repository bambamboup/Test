import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TypeFormation } from './type-formation.model';
import { TypeFormationPopupService } from './type-formation-popup.service';
import { TypeFormationService } from './type-formation.service';

@Component({
    selector: 'jhi-type-formation-dialog',
    templateUrl: './type-formation-dialog.component.html'
})
export class TypeFormationDialogComponent implements OnInit {

    typeFormation: TypeFormation;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private typeFormationService: TypeFormationService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.typeFormation.id !== undefined) {
            this.subscribeToSaveResponse(
                this.typeFormationService.update(this.typeFormation));
        } else {
            this.subscribeToSaveResponse(
                this.typeFormationService.create(this.typeFormation));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<TypeFormation>>) {
        result.subscribe((res: HttpResponse<TypeFormation>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: TypeFormation) {
        this.eventManager.broadcast({ name: 'typeFormationListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-type-formation-popup',
    template: ''
})
export class TypeFormationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private typeFormationPopupService: TypeFormationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.typeFormationPopupService
                    .open(TypeFormationDialogComponent as Component, params['id']);
            } else {
                this.typeFormationPopupService
                    .open(TypeFormationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
