import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { TypeFormation } from './type-formation.model';
import { TypeFormationService } from './type-formation.service';

@Component({
    selector: 'jhi-type-formation-detail',
    templateUrl: './type-formation-detail.component.html'
})
export class TypeFormationDetailComponent implements OnInit, OnDestroy {

    typeFormation: TypeFormation;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private typeFormationService: TypeFormationService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTypeFormations();
    }

    load(id) {
        this.typeFormationService.find(id)
            .subscribe((typeFormationResponse: HttpResponse<TypeFormation>) => {
                this.typeFormation = typeFormationResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTypeFormations() {
        this.eventSubscriber = this.eventManager.subscribe(
            'typeFormationListModification',
            (response) => this.load(this.typeFormation.id)
        );
    }
}
