import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { TypeFormationComponent } from './type-formation.component';
import { TypeFormationDetailComponent } from './type-formation-detail.component';
import { TypeFormationPopupComponent } from './type-formation-dialog.component';
import { TypeFormationDeletePopupComponent } from './type-formation-delete-dialog.component';

export const typeFormationRoute: Routes = [
    {
        path: 'type-formation',
        component: TypeFormationComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.typeFormation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'type-formation/:id',
        component: TypeFormationDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.typeFormation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const typeFormationPopupRoute: Routes = [
    {
        path: 'type-formation-new',
        component: TypeFormationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.typeFormation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'type-formation/:id/edit',
        component: TypeFormationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.typeFormation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'type-formation/:id/delete',
        component: TypeFormationDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.typeFormation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
