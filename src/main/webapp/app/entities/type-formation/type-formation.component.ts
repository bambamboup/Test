import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TypeFormation } from './type-formation.model';
import { TypeFormationService } from './type-formation.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-type-formation',
    templateUrl: './type-formation.component.html'
})
export class TypeFormationComponent implements OnInit, OnDestroy {
typeFormations: TypeFormation[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private typeFormationService: TypeFormationService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.typeFormationService.query().subscribe(
            (res: HttpResponse<TypeFormation[]>) => {
                this.typeFormations = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTypeFormations();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: TypeFormation) {
        return item.id;
    }
    registerChangeInTypeFormations() {
        this.eventSubscriber = this.eventManager.subscribe('typeFormationListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
