import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { TypeFormation } from './type-formation.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<TypeFormation>;

@Injectable()
export class TypeFormationService {

    private resourceUrl =  SERVER_API_URL + 'api/type-formations';

    constructor(private http: HttpClient) { }

    create(typeFormation: TypeFormation): Observable<EntityResponseType> {
        const copy = this.convert(typeFormation);
        return this.http.post<TypeFormation>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(typeFormation: TypeFormation): Observable<EntityResponseType> {
        const copy = this.convert(typeFormation);
        return this.http.put<TypeFormation>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<TypeFormation>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<TypeFormation[]>> {
        const options = createRequestOption(req);
        return this.http.get<TypeFormation[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TypeFormation[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: TypeFormation = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<TypeFormation[]>): HttpResponse<TypeFormation[]> {
        const jsonResponse: TypeFormation[] = res.body;
        const body: TypeFormation[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to TypeFormation.
     */
    private convertItemFromServer(typeFormation: TypeFormation): TypeFormation {
        const copy: TypeFormation = Object.assign({}, typeFormation);
        return copy;
    }

    /**
     * Convert a TypeFormation to a JSON which can be sent to the server.
     */
    private convert(typeFormation: TypeFormation): TypeFormation {
        const copy: TypeFormation = Object.assign({}, typeFormation);
        return copy;
    }
}
