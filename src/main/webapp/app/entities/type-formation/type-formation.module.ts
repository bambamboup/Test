import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestSharedModule } from '../../shared';
import {
    TypeFormationService,
    TypeFormationPopupService,
    TypeFormationComponent,
    TypeFormationDetailComponent,
    TypeFormationDialogComponent,
    TypeFormationPopupComponent,
    TypeFormationDeletePopupComponent,
    TypeFormationDeleteDialogComponent,
    typeFormationRoute,
    typeFormationPopupRoute,
} from './';

const ENTITY_STATES = [
    ...typeFormationRoute,
    ...typeFormationPopupRoute,
];

@NgModule({
    imports: [
        TestSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TypeFormationComponent,
        TypeFormationDetailComponent,
        TypeFormationDialogComponent,
        TypeFormationDeleteDialogComponent,
        TypeFormationPopupComponent,
        TypeFormationDeletePopupComponent,
    ],
    entryComponents: [
        TypeFormationComponent,
        TypeFormationDialogComponent,
        TypeFormationPopupComponent,
        TypeFormationDeleteDialogComponent,
        TypeFormationDeletePopupComponent,
    ],
    providers: [
        TypeFormationService,
        TypeFormationPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestTypeFormationModule {}
