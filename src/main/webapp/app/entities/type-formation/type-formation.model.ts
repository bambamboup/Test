import { BaseEntity } from './../../shared';

export const enum EnumTypeFormation {
    'FPCT',
    'FPCE'
}

export class TypeFormation implements BaseEntity {
    constructor(
        public id?: number,
        public typeFormat?: EnumTypeFormation,
        public formations?: BaseEntity[],
    ) {
    }
}
