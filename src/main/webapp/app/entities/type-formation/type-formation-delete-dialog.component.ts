import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TypeFormation } from './type-formation.model';
import { TypeFormationPopupService } from './type-formation-popup.service';
import { TypeFormationService } from './type-formation.service';

@Component({
    selector: 'jhi-type-formation-delete-dialog',
    templateUrl: './type-formation-delete-dialog.component.html'
})
export class TypeFormationDeleteDialogComponent {

    typeFormation: TypeFormation;

    constructor(
        private typeFormationService: TypeFormationService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.typeFormationService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'typeFormationListModification',
                content: 'Deleted an typeFormation'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-type-formation-delete-popup',
    template: ''
})
export class TypeFormationDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private typeFormationPopupService: TypeFormationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.typeFormationPopupService
                .open(TypeFormationDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
