import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Journal } from './journal.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Journal>;

@Injectable()
export class JournalService {

    private resourceUrl =  SERVER_API_URL + 'api/journals';

    constructor(private http: HttpClient) { }

    create(journal: Journal): Observable<EntityResponseType> {
        const copy = this.convert(journal);
        return this.http.post<Journal>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(journal: Journal): Observable<EntityResponseType> {
        const copy = this.convert(journal);
        return this.http.put<Journal>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Journal>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Journal[]>> {
        const options = createRequestOption(req);
        return this.http.get<Journal[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Journal[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Journal = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Journal[]>): HttpResponse<Journal[]> {
        const jsonResponse: Journal[] = res.body;
        const body: Journal[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Journal.
     */
    private convertItemFromServer(journal: Journal): Journal {
        const copy: Journal = Object.assign({}, journal);
        return copy;
    }

    /**
     * Convert a Journal to a JSON which can be sent to the server.
     */
    private convert(journal: Journal): Journal {
        const copy: Journal = Object.assign({}, journal);
        return copy;
    }
}
