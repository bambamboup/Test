import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Journal } from './journal.model';
import { JournalService } from './journal.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-journal',
    templateUrl: './journal.component.html'
})
export class JournalComponent implements OnInit, OnDestroy {
journals: Journal[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private journalService: JournalService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.journalService.query().subscribe(
            (res: HttpResponse<Journal[]>) => {
                this.journals = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInJournals();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Journal) {
        return item.id;
    }
    registerChangeInJournals() {
        this.eventSubscriber = this.eventManager.subscribe('journalListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
