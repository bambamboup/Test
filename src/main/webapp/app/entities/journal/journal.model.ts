import { BaseEntity } from './../../shared';

export class Journal implements BaseEntity {
    constructor(
        public id?: number,
        public codeJournal?: string,
        public nomJournal?: string,
    ) {
    }
}
