import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JournalComponent } from './journal.component';
import { JournalDetailComponent } from './journal-detail.component';
import { JournalPopupComponent } from './journal-dialog.component';
import { JournalDeletePopupComponent } from './journal-delete-dialog.component';

export const journalRoute: Routes = [
    {
        path: 'journal',
        component: JournalComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.journal.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'journal/:id',
        component: JournalDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.journal.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const journalPopupRoute: Routes = [
    {
        path: 'journal-new',
        component: JournalPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.journal.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'journal/:id/edit',
        component: JournalPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.journal.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'journal/:id/delete',
        component: JournalDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.journal.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
