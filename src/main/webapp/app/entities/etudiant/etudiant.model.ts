import { BaseEntity, User } from './../../shared';

export const enum EnumGenre {
    'MASCULIN',
    'FEMININ'
}

export class Etudiant implements BaseEntity {
    constructor(
        public id?: number,
        public genre?: EnumGenre,
        public dateNaissance?: any,
        public lieuNaissance?: string,
        public adresse?: string,
        public telephone?: string,
        public priseEnCharge?: boolean,
        public malade?: boolean,
        public nationalite?: BaseEntity,
        public user?: User,
    ) {
        this.priseEnCharge = false;
        this.malade = false;
    }
}
