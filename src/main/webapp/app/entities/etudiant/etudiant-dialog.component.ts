import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Etudiant } from './etudiant.model';
import { EtudiantPopupService } from './etudiant-popup.service';
import { EtudiantService } from './etudiant.service';
import { Nationalite, NationaliteService } from '../nationalite';
import { User, UserService } from '../../shared';

@Component({
    selector: 'jhi-etudiant-dialog',
    templateUrl: './etudiant-dialog.component.html'
})
export class EtudiantDialogComponent implements OnInit {

    etudiant: Etudiant;
    isSaving: boolean;

    nationalites: Nationalite[];

    users: User[];
    dateNaissanceDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private etudiantService: EtudiantService,
        private nationaliteService: NationaliteService,
        private userService: UserService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.nationaliteService.query()
            .subscribe((res: HttpResponse<Nationalite[]>) => { this.nationalites = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.userService.query()
            .subscribe((res: HttpResponse<User[]>) => { this.users = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.etudiant.id !== undefined) {
            this.subscribeToSaveResponse(
                this.etudiantService.update(this.etudiant));
        } else {
            this.subscribeToSaveResponse(
                this.etudiantService.create(this.etudiant));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Etudiant>>) {
        result.subscribe((res: HttpResponse<Etudiant>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Etudiant) {
        this.eventManager.broadcast({ name: 'etudiantListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackNationaliteById(index: number, item: Nationalite) {
        return item.id;
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-etudiant-popup',
    template: ''
})
export class EtudiantPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private etudiantPopupService: EtudiantPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.etudiantPopupService
                    .open(EtudiantDialogComponent as Component, params['id']);
            } else {
                this.etudiantPopupService
                    .open(EtudiantDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
