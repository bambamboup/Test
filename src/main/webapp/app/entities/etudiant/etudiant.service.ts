import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Etudiant } from './etudiant.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Etudiant>;

@Injectable()
export class EtudiantService {

    private resourceUrl =  SERVER_API_URL + 'api/etudiants';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(etudiant: Etudiant): Observable<EntityResponseType> {
        const copy = this.convert(etudiant);
        return this.http.post<Etudiant>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(etudiant: Etudiant): Observable<EntityResponseType> {
        const copy = this.convert(etudiant);
        return this.http.put<Etudiant>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Etudiant>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Etudiant[]>> {
        const options = createRequestOption(req);
        return this.http.get<Etudiant[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Etudiant[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Etudiant = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Etudiant[]>): HttpResponse<Etudiant[]> {
        const jsonResponse: Etudiant[] = res.body;
        const body: Etudiant[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Etudiant.
     */
    private convertItemFromServer(etudiant: Etudiant): Etudiant {
        const copy: Etudiant = Object.assign({}, etudiant);
        copy.dateNaissance = this.dateUtils
            .convertLocalDateFromServer(etudiant.dateNaissance);
        return copy;
    }

    /**
     * Convert a Etudiant to a JSON which can be sent to the server.
     */
    private convert(etudiant: Etudiant): Etudiant {
        const copy: Etudiant = Object.assign({}, etudiant);
        copy.dateNaissance = this.dateUtils
            .convertLocalDateToServer(etudiant.dateNaissance);
        return copy;
    }
}
