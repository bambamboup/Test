import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Fournisseur } from './fournisseur.model';
import { FournisseurService } from './fournisseur.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-fournisseur',
    templateUrl: './fournisseur.component.html'
})
export class FournisseurComponent implements OnInit, OnDestroy {
fournisseurs: Fournisseur[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private fournisseurService: FournisseurService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.fournisseurService.query().subscribe(
            (res: HttpResponse<Fournisseur[]>) => {
                this.fournisseurs = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInFournisseurs();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Fournisseur) {
        return item.id;
    }
    registerChangeInFournisseurs() {
        this.eventSubscriber = this.eventManager.subscribe('fournisseurListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
