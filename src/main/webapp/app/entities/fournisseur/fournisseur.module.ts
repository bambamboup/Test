import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestSharedModule } from '../../shared';
import {
    FournisseurService,
    FournisseurPopupService,
    FournisseurComponent,
    FournisseurDetailComponent,
    FournisseurDialogComponent,
    FournisseurPopupComponent,
    FournisseurDeletePopupComponent,
    FournisseurDeleteDialogComponent,
    fournisseurRoute,
    fournisseurPopupRoute,
} from './';

const ENTITY_STATES = [
    ...fournisseurRoute,
    ...fournisseurPopupRoute,
];

@NgModule({
    imports: [
        TestSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        FournisseurComponent,
        FournisseurDetailComponent,
        FournisseurDialogComponent,
        FournisseurDeleteDialogComponent,
        FournisseurPopupComponent,
        FournisseurDeletePopupComponent,
    ],
    entryComponents: [
        FournisseurComponent,
        FournisseurDialogComponent,
        FournisseurPopupComponent,
        FournisseurDeleteDialogComponent,
        FournisseurDeletePopupComponent,
    ],
    providers: [
        FournisseurService,
        FournisseurPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestFournisseurModule {}
