import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { FournisseurComponent } from './fournisseur.component';
import { FournisseurDetailComponent } from './fournisseur-detail.component';
import { FournisseurPopupComponent } from './fournisseur-dialog.component';
import { FournisseurDeletePopupComponent } from './fournisseur-delete-dialog.component';

export const fournisseurRoute: Routes = [
    {
        path: 'fournisseur',
        component: FournisseurComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.fournisseur.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'fournisseur/:id',
        component: FournisseurDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.fournisseur.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const fournisseurPopupRoute: Routes = [
    {
        path: 'fournisseur-new',
        component: FournisseurPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.fournisseur.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'fournisseur/:id/edit',
        component: FournisseurPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.fournisseur.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'fournisseur/:id/delete',
        component: FournisseurDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.fournisseur.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
