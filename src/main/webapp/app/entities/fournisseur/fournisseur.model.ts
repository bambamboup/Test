import { BaseEntity } from './../../shared';

export class Fournisseur implements BaseEntity {
    constructor(
        public id?: number,
        public codeFournisseur?: string,
        public adresse?: string,
        public nomFournisseur?: string,
        public telephone?: string,
    ) {
    }
}
