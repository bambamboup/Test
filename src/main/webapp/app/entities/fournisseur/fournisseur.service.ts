import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Fournisseur } from './fournisseur.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Fournisseur>;

@Injectable()
export class FournisseurService {

    private resourceUrl =  SERVER_API_URL + 'api/fournisseurs';

    constructor(private http: HttpClient) { }

    create(fournisseur: Fournisseur): Observable<EntityResponseType> {
        const copy = this.convert(fournisseur);
        return this.http.post<Fournisseur>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(fournisseur: Fournisseur): Observable<EntityResponseType> {
        const copy = this.convert(fournisseur);
        return this.http.put<Fournisseur>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Fournisseur>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Fournisseur[]>> {
        const options = createRequestOption(req);
        return this.http.get<Fournisseur[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Fournisseur[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Fournisseur = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Fournisseur[]>): HttpResponse<Fournisseur[]> {
        const jsonResponse: Fournisseur[] = res.body;
        const body: Fournisseur[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Fournisseur.
     */
    private convertItemFromServer(fournisseur: Fournisseur): Fournisseur {
        const copy: Fournisseur = Object.assign({}, fournisseur);
        return copy;
    }

    /**
     * Convert a Fournisseur to a JSON which can be sent to the server.
     */
    private convert(fournisseur: Fournisseur): Fournisseur {
        const copy: Fournisseur = Object.assign({}, fournisseur);
        return copy;
    }
}
