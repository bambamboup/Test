import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { TiersPayant } from './tiers-payant.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<TiersPayant>;

@Injectable()
export class TiersPayantService {

    private resourceUrl =  SERVER_API_URL + 'api/tiers-payants';

    constructor(private http: HttpClient) { }

    create(tiersPayant: TiersPayant): Observable<EntityResponseType> {
        const copy = this.convert(tiersPayant);
        return this.http.post<TiersPayant>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(tiersPayant: TiersPayant): Observable<EntityResponseType> {
        const copy = this.convert(tiersPayant);
        return this.http.put<TiersPayant>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<TiersPayant>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<TiersPayant[]>> {
        const options = createRequestOption(req);
        return this.http.get<TiersPayant[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TiersPayant[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: TiersPayant = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<TiersPayant[]>): HttpResponse<TiersPayant[]> {
        const jsonResponse: TiersPayant[] = res.body;
        const body: TiersPayant[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to TiersPayant.
     */
    private convertItemFromServer(tiersPayant: TiersPayant): TiersPayant {
        const copy: TiersPayant = Object.assign({}, tiersPayant);
        return copy;
    }

    /**
     * Convert a TiersPayant to a JSON which can be sent to the server.
     */
    private convert(tiersPayant: TiersPayant): TiersPayant {
        const copy: TiersPayant = Object.assign({}, tiersPayant);
        return copy;
    }
}
