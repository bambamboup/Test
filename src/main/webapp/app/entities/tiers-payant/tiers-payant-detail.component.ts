import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { TiersPayant } from './tiers-payant.model';
import { TiersPayantService } from './tiers-payant.service';

@Component({
    selector: 'jhi-tiers-payant-detail',
    templateUrl: './tiers-payant-detail.component.html'
})
export class TiersPayantDetailComponent implements OnInit, OnDestroy {

    tiersPayant: TiersPayant;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private tiersPayantService: TiersPayantService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTiersPayants();
    }

    load(id) {
        this.tiersPayantService.find(id)
            .subscribe((tiersPayantResponse: HttpResponse<TiersPayant>) => {
                this.tiersPayant = tiersPayantResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTiersPayants() {
        this.eventSubscriber = this.eventManager.subscribe(
            'tiersPayantListModification',
            (response) => this.load(this.tiersPayant.id)
        );
    }
}
