import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TiersPayant } from './tiers-payant.model';
import { TiersPayantPopupService } from './tiers-payant-popup.service';
import { TiersPayantService } from './tiers-payant.service';

@Component({
    selector: 'jhi-tiers-payant-delete-dialog',
    templateUrl: './tiers-payant-delete-dialog.component.html'
})
export class TiersPayantDeleteDialogComponent {

    tiersPayant: TiersPayant;

    constructor(
        private tiersPayantService: TiersPayantService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.tiersPayantService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'tiersPayantListModification',
                content: 'Deleted an tiersPayant'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-tiers-payant-delete-popup',
    template: ''
})
export class TiersPayantDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tiersPayantPopupService: TiersPayantPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.tiersPayantPopupService
                .open(TiersPayantDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
