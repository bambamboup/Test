import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { TiersPayant } from './tiers-payant.model';
import { TiersPayantService } from './tiers-payant.service';

@Injectable()
export class TiersPayantPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private tiersPayantService: TiersPayantService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.tiersPayantService.find(id)
                    .subscribe((tiersPayantResponse: HttpResponse<TiersPayant>) => {
                        const tiersPayant: TiersPayant = tiersPayantResponse.body;
                        this.ngbModalRef = this.tiersPayantModalRef(component, tiersPayant);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.tiersPayantModalRef(component, new TiersPayant());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    tiersPayantModalRef(component: Component, tiersPayant: TiersPayant): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.tiersPayant = tiersPayant;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
