import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TiersPayant } from './tiers-payant.model';
import { TiersPayantService } from './tiers-payant.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-tiers-payant',
    templateUrl: './tiers-payant.component.html'
})
export class TiersPayantComponent implements OnInit, OnDestroy {
tiersPayants: TiersPayant[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private tiersPayantService: TiersPayantService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.tiersPayantService.query().subscribe(
            (res: HttpResponse<TiersPayant[]>) => {
                this.tiersPayants = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTiersPayants();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: TiersPayant) {
        return item.id;
    }
    registerChangeInTiersPayants() {
        this.eventSubscriber = this.eventManager.subscribe('tiersPayantListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
