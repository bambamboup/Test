import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TiersPayant } from './tiers-payant.model';
import { TiersPayantPopupService } from './tiers-payant-popup.service';
import { TiersPayantService } from './tiers-payant.service';

@Component({
    selector: 'jhi-tiers-payant-dialog',
    templateUrl: './tiers-payant-dialog.component.html'
})
export class TiersPayantDialogComponent implements OnInit {

    tiersPayant: TiersPayant;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private tiersPayantService: TiersPayantService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.tiersPayant.id !== undefined) {
            this.subscribeToSaveResponse(
                this.tiersPayantService.update(this.tiersPayant));
        } else {
            this.subscribeToSaveResponse(
                this.tiersPayantService.create(this.tiersPayant));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<TiersPayant>>) {
        result.subscribe((res: HttpResponse<TiersPayant>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: TiersPayant) {
        this.eventManager.broadcast({ name: 'tiersPayantListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-tiers-payant-popup',
    template: ''
})
export class TiersPayantPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tiersPayantPopupService: TiersPayantPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.tiersPayantPopupService
                    .open(TiersPayantDialogComponent as Component, params['id']);
            } else {
                this.tiersPayantPopupService
                    .open(TiersPayantDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
