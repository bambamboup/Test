import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestSharedModule } from '../../shared';
import {
    TiersPayantService,
    TiersPayantPopupService,
    TiersPayantComponent,
    TiersPayantDetailComponent,
    TiersPayantDialogComponent,
    TiersPayantPopupComponent,
    TiersPayantDeletePopupComponent,
    TiersPayantDeleteDialogComponent,
    tiersPayantRoute,
    tiersPayantPopupRoute,
} from './';

const ENTITY_STATES = [
    ...tiersPayantRoute,
    ...tiersPayantPopupRoute,
];

@NgModule({
    imports: [
        TestSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TiersPayantComponent,
        TiersPayantDetailComponent,
        TiersPayantDialogComponent,
        TiersPayantDeleteDialogComponent,
        TiersPayantPopupComponent,
        TiersPayantDeletePopupComponent,
    ],
    entryComponents: [
        TiersPayantComponent,
        TiersPayantDialogComponent,
        TiersPayantPopupComponent,
        TiersPayantDeleteDialogComponent,
        TiersPayantDeletePopupComponent,
    ],
    providers: [
        TiersPayantService,
        TiersPayantPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestTiersPayantModule {}
