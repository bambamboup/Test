export * from './tiers-payant.model';
export * from './tiers-payant-popup.service';
export * from './tiers-payant.service';
export * from './tiers-payant-dialog.component';
export * from './tiers-payant-delete-dialog.component';
export * from './tiers-payant-detail.component';
export * from './tiers-payant.component';
export * from './tiers-payant.route';
