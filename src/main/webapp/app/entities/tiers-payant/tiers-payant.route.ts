import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { TiersPayantComponent } from './tiers-payant.component';
import { TiersPayantDetailComponent } from './tiers-payant-detail.component';
import { TiersPayantPopupComponent } from './tiers-payant-dialog.component';
import { TiersPayantDeletePopupComponent } from './tiers-payant-delete-dialog.component';

export const tiersPayantRoute: Routes = [
    {
        path: 'tiers-payant',
        component: TiersPayantComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.tiersPayant.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tiers-payant/:id',
        component: TiersPayantDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.tiersPayant.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const tiersPayantPopupRoute: Routes = [
    {
        path: 'tiers-payant-new',
        component: TiersPayantPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.tiersPayant.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tiers-payant/:id/edit',
        component: TiersPayantPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.tiersPayant.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tiers-payant/:id/delete',
        component: TiersPayantDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.tiersPayant.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
