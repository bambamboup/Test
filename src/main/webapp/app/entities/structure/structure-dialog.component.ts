import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Structure } from './structure.model';
import { StructurePopupService } from './structure-popup.service';
import { StructureService } from './structure.service';

@Component({
    selector: 'jhi-structure-dialog',
    templateUrl: './structure-dialog.component.html'
})
export class StructureDialogComponent implements OnInit {

    structure: Structure;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private structureService: StructureService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.structure.id !== undefined) {
            this.subscribeToSaveResponse(
                this.structureService.update(this.structure));
        } else {
            this.subscribeToSaveResponse(
                this.structureService.create(this.structure));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Structure>>) {
        result.subscribe((res: HttpResponse<Structure>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Structure) {
        this.eventManager.broadcast({ name: 'structureListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-structure-popup',
    template: ''
})
export class StructurePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private structurePopupService: StructurePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.structurePopupService
                    .open(StructureDialogComponent as Component, params['id']);
            } else {
                this.structurePopupService
                    .open(StructureDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
