import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { StructureComponent } from './structure.component';
import { StructureDetailComponent } from './structure-detail.component';
import { StructurePopupComponent } from './structure-dialog.component';
import { StructureDeletePopupComponent } from './structure-delete-dialog.component';

export const structureRoute: Routes = [
    {
        path: 'structure',
        component: StructureComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.structure.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'structure/:id',
        component: StructureDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.structure.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const structurePopupRoute: Routes = [
    {
        path: 'structure-new',
        component: StructurePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.structure.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'structure/:id/edit',
        component: StructurePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.structure.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'structure/:id/delete',
        component: StructureDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.structure.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
