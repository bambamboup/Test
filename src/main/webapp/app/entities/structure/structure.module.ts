import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestSharedModule } from '../../shared';
import {
    StructureService,
    StructurePopupService,
    StructureComponent,
    StructureDetailComponent,
    StructureDialogComponent,
    StructurePopupComponent,
    StructureDeletePopupComponent,
    StructureDeleteDialogComponent,
    structureRoute,
    structurePopupRoute,
} from './';

const ENTITY_STATES = [
    ...structureRoute,
    ...structurePopupRoute,
];

@NgModule({
    imports: [
        TestSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        StructureComponent,
        StructureDetailComponent,
        StructureDialogComponent,
        StructureDeleteDialogComponent,
        StructurePopupComponent,
        StructureDeletePopupComponent,
    ],
    entryComponents: [
        StructureComponent,
        StructureDialogComponent,
        StructurePopupComponent,
        StructureDeleteDialogComponent,
        StructureDeletePopupComponent,
    ],
    providers: [
        StructureService,
        StructurePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestStructureModule {}
