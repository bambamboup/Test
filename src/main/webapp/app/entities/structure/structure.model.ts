import { BaseEntity } from './../../shared';

export class Structure implements BaseEntity {
    constructor(
        public id?: number,
        public codeStructure?: string,
        public nomStructure?: string,
        public formations?: BaseEntity[],
    ) {
    }
}
