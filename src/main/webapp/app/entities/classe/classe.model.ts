import { BaseEntity } from './../../shared';

export const enum EnumNivLMD {
    'L1',
    'L2',
    'L3',
    'M1',
    'M2'
}

export class Classe implements BaseEntity {
    constructor(
        public id?: number,
        public codeClasse?: string,
        public libelle?: string,
        public terminal?: boolean,
        public nivLMD?: EnumNivLMD,
        public fraisInscription?: number,
        public entrant?: boolean,
        public sortant?: boolean,
        public inscriptions?: BaseEntity[],
        public formation?: BaseEntity,
    ) {
        this.terminal = false;
        this.entrant = false;
        this.sortant = false;
    }
}
