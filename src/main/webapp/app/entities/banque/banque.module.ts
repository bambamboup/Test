import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestSharedModule } from '../../shared';
import {
    BanqueService,
    BanquePopupService,
    BanqueComponent,
    BanqueDetailComponent,
    BanqueDialogComponent,
    BanquePopupComponent,
    BanqueDeletePopupComponent,
    BanqueDeleteDialogComponent,
    banqueRoute,
    banquePopupRoute,
} from './';

const ENTITY_STATES = [
    ...banqueRoute,
    ...banquePopupRoute,
];

@NgModule({
    imports: [
        TestSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        BanqueComponent,
        BanqueDetailComponent,
        BanqueDialogComponent,
        BanqueDeleteDialogComponent,
        BanquePopupComponent,
        BanqueDeletePopupComponent,
    ],
    entryComponents: [
        BanqueComponent,
        BanqueDialogComponent,
        BanquePopupComponent,
        BanqueDeleteDialogComponent,
        BanqueDeletePopupComponent,
    ],
    providers: [
        BanqueService,
        BanquePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestBanqueModule {}
