import { BaseEntity } from './../../shared';

export class Banque implements BaseEntity {
    constructor(
        public id?: number,
        public codeBanque?: string,
        public nomBanque?: string,
    ) {
    }
}
