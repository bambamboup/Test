import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Banque } from './banque.model';
import { BanqueService } from './banque.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-banque',
    templateUrl: './banque.component.html'
})
export class BanqueComponent implements OnInit, OnDestroy {
banques: Banque[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private banqueService: BanqueService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.banqueService.query().subscribe(
            (res: HttpResponse<Banque[]>) => {
                this.banques = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInBanques();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Banque) {
        return item.id;
    }
    registerChangeInBanques() {
        this.eventSubscriber = this.eventManager.subscribe('banqueListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
