import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Banque } from './banque.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Banque>;

@Injectable()
export class BanqueService {

    private resourceUrl =  SERVER_API_URL + 'api/banques';

    constructor(private http: HttpClient) { }

    create(banque: Banque): Observable<EntityResponseType> {
        const copy = this.convert(banque);
        return this.http.post<Banque>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(banque: Banque): Observable<EntityResponseType> {
        const copy = this.convert(banque);
        return this.http.put<Banque>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Banque>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Banque[]>> {
        const options = createRequestOption(req);
        return this.http.get<Banque[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Banque[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Banque = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Banque[]>): HttpResponse<Banque[]> {
        const jsonResponse: Banque[] = res.body;
        const body: Banque[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Banque.
     */
    private convertItemFromServer(banque: Banque): Banque {
        const copy: Banque = Object.assign({}, banque);
        return copy;
    }

    /**
     * Convert a Banque to a JSON which can be sent to the server.
     */
    private convert(banque: Banque): Banque {
        const copy: Banque = Object.assign({}, banque);
        return copy;
    }
}
