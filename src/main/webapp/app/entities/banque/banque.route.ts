import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { BanqueComponent } from './banque.component';
import { BanqueDetailComponent } from './banque-detail.component';
import { BanquePopupComponent } from './banque-dialog.component';
import { BanqueDeletePopupComponent } from './banque-delete-dialog.component';

export const banqueRoute: Routes = [
    {
        path: 'banque',
        component: BanqueComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.banque.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'banque/:id',
        component: BanqueDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.banque.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const banquePopupRoute: Routes = [
    {
        path: 'banque-new',
        component: BanquePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.banque.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'banque/:id/edit',
        component: BanquePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.banque.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'banque/:id/delete',
        component: BanqueDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.banque.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
