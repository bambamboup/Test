import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Banque } from './banque.model';
import { BanqueService } from './banque.service';

@Component({
    selector: 'jhi-banque-detail',
    templateUrl: './banque-detail.component.html'
})
export class BanqueDetailComponent implements OnInit, OnDestroy {

    banque: Banque;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private banqueService: BanqueService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInBanques();
    }

    load(id) {
        this.banqueService.find(id)
            .subscribe((banqueResponse: HttpResponse<Banque>) => {
                this.banque = banqueResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInBanques() {
        this.eventSubscriber = this.eventManager.subscribe(
            'banqueListModification',
            (response) => this.load(this.banque.id)
        );
    }
}
