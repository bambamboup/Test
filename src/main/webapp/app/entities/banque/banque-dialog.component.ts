import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Banque } from './banque.model';
import { BanquePopupService } from './banque-popup.service';
import { BanqueService } from './banque.service';

@Component({
    selector: 'jhi-banque-dialog',
    templateUrl: './banque-dialog.component.html'
})
export class BanqueDialogComponent implements OnInit {

    banque: Banque;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private banqueService: BanqueService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.banque.id !== undefined) {
            this.subscribeToSaveResponse(
                this.banqueService.update(this.banque));
        } else {
            this.subscribeToSaveResponse(
                this.banqueService.create(this.banque));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Banque>>) {
        result.subscribe((res: HttpResponse<Banque>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Banque) {
        this.eventManager.broadcast({ name: 'banqueListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-banque-popup',
    template: ''
})
export class BanquePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private banquePopupService: BanquePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.banquePopupService
                    .open(BanqueDialogComponent as Component, params['id']);
            } else {
                this.banquePopupService
                    .open(BanqueDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
