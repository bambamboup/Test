import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { MembreComponent } from './membre.component';
import { MembreDetailComponent } from './membre-detail.component';
import { MembrePopupComponent } from './membre-dialog.component';
import { MembreDeletePopupComponent } from './membre-delete-dialog.component';

export const membreRoute: Routes = [
    {
        path: 'membre',
        component: MembreComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.membre.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'membre/:id',
        component: MembreDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.membre.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const membrePopupRoute: Routes = [
    {
        path: 'membre-new',
        component: MembrePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.membre.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'membre/:id/edit',
        component: MembrePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.membre.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'membre/:id/delete',
        component: MembreDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.membre.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
