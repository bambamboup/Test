import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Membre } from './membre.model';
import { MembrePopupService } from './membre-popup.service';
import { MembreService } from './membre.service';

@Component({
    selector: 'jhi-membre-delete-dialog',
    templateUrl: './membre-delete-dialog.component.html'
})
export class MembreDeleteDialogComponent {

    membre: Membre;

    constructor(
        private membreService: MembreService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.membreService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'membreListModification',
                content: 'Deleted an membre'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-membre-delete-popup',
    template: ''
})
export class MembreDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private membrePopupService: MembrePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.membrePopupService
                .open(MembreDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
