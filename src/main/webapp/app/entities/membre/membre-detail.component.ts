import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Membre } from './membre.model';
import { MembreService } from './membre.service';

@Component({
    selector: 'jhi-membre-detail',
    templateUrl: './membre-detail.component.html'
})
export class MembreDetailComponent implements OnInit, OnDestroy {

    membre: Membre;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private membreService: MembreService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInMembres();
    }

    load(id) {
        this.membreService.find(id)
            .subscribe((membreResponse: HttpResponse<Membre>) => {
                this.membre = membreResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInMembres() {
        this.eventSubscriber = this.eventManager.subscribe(
            'membreListModification',
            (response) => this.load(this.membre.id)
        );
    }
}
