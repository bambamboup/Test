import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Membre } from './membre.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Membre>;

@Injectable()
export class MembreService {

    private resourceUrl =  SERVER_API_URL + 'api/membres';

    constructor(private http: HttpClient) { }

    create(membre: Membre): Observable<EntityResponseType> {
        const copy = this.convert(membre);
        return this.http.post<Membre>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(membre: Membre): Observable<EntityResponseType> {
        const copy = this.convert(membre);
        return this.http.put<Membre>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Membre>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Membre[]>> {
        const options = createRequestOption(req);
        return this.http.get<Membre[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Membre[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Membre = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Membre[]>): HttpResponse<Membre[]> {
        const jsonResponse: Membre[] = res.body;
        const body: Membre[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Membre.
     */
    private convertItemFromServer(membre: Membre): Membre {
        const copy: Membre = Object.assign({}, membre);
        return copy;
    }

    /**
     * Convert a Membre to a JSON which can be sent to the server.
     */
    private convert(membre: Membre): Membre {
        const copy: Membre = Object.assign({}, membre);
        return copy;
    }
}
