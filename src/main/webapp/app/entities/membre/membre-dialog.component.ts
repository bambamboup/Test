import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Membre } from './membre.model';
import { MembrePopupService } from './membre-popup.service';
import { MembreService } from './membre.service';
import { User, UserService } from '../../shared';

@Component({
    selector: 'jhi-membre-dialog',
    templateUrl: './membre-dialog.component.html'
})
export class MembreDialogComponent implements OnInit {

    membre: Membre;
    isSaving: boolean;

    users: User[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private membreService: MembreService,
        private userService: UserService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.userService.query()
            .subscribe((res: HttpResponse<User[]>) => { this.users = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.membre.id !== undefined) {
            this.subscribeToSaveResponse(
                this.membreService.update(this.membre));
        } else {
            this.subscribeToSaveResponse(
                this.membreService.create(this.membre));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Membre>>) {
        result.subscribe((res: HttpResponse<Membre>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Membre) {
        this.eventManager.broadcast({ name: 'membreListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-membre-popup',
    template: ''
})
export class MembrePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private membrePopupService: MembrePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.membrePopupService
                    .open(MembreDialogComponent as Component, params['id']);
            } else {
                this.membrePopupService
                    .open(MembreDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
