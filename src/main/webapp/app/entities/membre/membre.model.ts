import { BaseEntity, User } from './../../shared';

export const enum EnumMembre {
    'PATS',
    'PER'
}

export class Membre implements BaseEntity {
    constructor(
        public id?: number,
        public matricule?: string,
        public telephone?: string,
        public typeMembre?: EnumMembre,
        public user?: User,
        public formations?: BaseEntity[],
    ) {
    }
}
