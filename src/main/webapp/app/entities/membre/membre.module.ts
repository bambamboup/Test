import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestSharedModule } from '../../shared';
import { TestAdminModule } from '../../admin/admin.module';
import {
    MembreService,
    MembrePopupService,
    MembreComponent,
    MembreDetailComponent,
    MembreDialogComponent,
    MembrePopupComponent,
    MembreDeletePopupComponent,
    MembreDeleteDialogComponent,
    membreRoute,
    membrePopupRoute,
} from './';

const ENTITY_STATES = [
    ...membreRoute,
    ...membrePopupRoute,
];

@NgModule({
    imports: [
        TestSharedModule,
        TestAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        MembreComponent,
        MembreDetailComponent,
        MembreDialogComponent,
        MembreDeleteDialogComponent,
        MembrePopupComponent,
        MembreDeletePopupComponent,
    ],
    entryComponents: [
        MembreComponent,
        MembreDialogComponent,
        MembrePopupComponent,
        MembreDeleteDialogComponent,
        MembreDeletePopupComponent,
    ],
    providers: [
        MembreService,
        MembrePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestMembreModule {}
