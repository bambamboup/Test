import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AnneeAccademique } from './annee-accademique.model';
import { AnneeAccademiquePopupService } from './annee-accademique-popup.service';
import { AnneeAccademiqueService } from './annee-accademique.service';

@Component({
    selector: 'jhi-annee-accademique-dialog',
    templateUrl: './annee-accademique-dialog.component.html'
})
export class AnneeAccademiqueDialogComponent implements OnInit {

    anneeAccademique: AnneeAccademique;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private anneeAccademiqueService: AnneeAccademiqueService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.anneeAccademique.id !== undefined) {
            this.subscribeToSaveResponse(
                this.anneeAccademiqueService.update(this.anneeAccademique));
        } else {
            this.subscribeToSaveResponse(
                this.anneeAccademiqueService.create(this.anneeAccademique));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<AnneeAccademique>>) {
        result.subscribe((res: HttpResponse<AnneeAccademique>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: AnneeAccademique) {
        this.eventManager.broadcast({ name: 'anneeAccademiqueListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-annee-accademique-popup',
    template: ''
})
export class AnneeAccademiquePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private anneeAccademiquePopupService: AnneeAccademiquePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.anneeAccademiquePopupService
                    .open(AnneeAccademiqueDialogComponent as Component, params['id']);
            } else {
                this.anneeAccademiquePopupService
                    .open(AnneeAccademiqueDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
