import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { AnneeAccademique } from './annee-accademique.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<AnneeAccademique>;

@Injectable()
export class AnneeAccademiqueService {

    private resourceUrl =  SERVER_API_URL + 'api/annee-accademiques';

    constructor(private http: HttpClient) { }

    create(anneeAccademique: AnneeAccademique): Observable<EntityResponseType> {
        const copy = this.convert(anneeAccademique);
        return this.http.post<AnneeAccademique>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(anneeAccademique: AnneeAccademique): Observable<EntityResponseType> {
        const copy = this.convert(anneeAccademique);
        return this.http.put<AnneeAccademique>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<AnneeAccademique>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<AnneeAccademique[]>> {
        const options = createRequestOption(req);
        return this.http.get<AnneeAccademique[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<AnneeAccademique[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: AnneeAccademique = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<AnneeAccademique[]>): HttpResponse<AnneeAccademique[]> {
        const jsonResponse: AnneeAccademique[] = res.body;
        const body: AnneeAccademique[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to AnneeAccademique.
     */
    private convertItemFromServer(anneeAccademique: AnneeAccademique): AnneeAccademique {
        const copy: AnneeAccademique = Object.assign({}, anneeAccademique);
        return copy;
    }

    /**
     * Convert a AnneeAccademique to a JSON which can be sent to the server.
     */
    private convert(anneeAccademique: AnneeAccademique): AnneeAccademique {
        const copy: AnneeAccademique = Object.assign({}, anneeAccademique);
        return copy;
    }
}
