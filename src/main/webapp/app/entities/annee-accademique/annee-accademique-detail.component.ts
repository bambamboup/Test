import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { AnneeAccademique } from './annee-accademique.model';
import { AnneeAccademiqueService } from './annee-accademique.service';

@Component({
    selector: 'jhi-annee-accademique-detail',
    templateUrl: './annee-accademique-detail.component.html'
})
export class AnneeAccademiqueDetailComponent implements OnInit, OnDestroy {

    anneeAccademique: AnneeAccademique;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private anneeAccademiqueService: AnneeAccademiqueService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAnneeAccademiques();
    }

    load(id) {
        this.anneeAccademiqueService.find(id)
            .subscribe((anneeAccademiqueResponse: HttpResponse<AnneeAccademique>) => {
                this.anneeAccademique = anneeAccademiqueResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAnneeAccademiques() {
        this.eventSubscriber = this.eventManager.subscribe(
            'anneeAccademiqueListModification',
            (response) => this.load(this.anneeAccademique.id)
        );
    }
}
