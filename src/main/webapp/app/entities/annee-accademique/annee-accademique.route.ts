import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { AnneeAccademiqueComponent } from './annee-accademique.component';
import { AnneeAccademiqueDetailComponent } from './annee-accademique-detail.component';
import { AnneeAccademiquePopupComponent } from './annee-accademique-dialog.component';
import { AnneeAccademiqueDeletePopupComponent } from './annee-accademique-delete-dialog.component';

export const anneeAccademiqueRoute: Routes = [
    {
        path: 'annee-accademique',
        component: AnneeAccademiqueComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.anneeAccademique.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'annee-accademique/:id',
        component: AnneeAccademiqueDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.anneeAccademique.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const anneeAccademiquePopupRoute: Routes = [
    {
        path: 'annee-accademique-new',
        component: AnneeAccademiquePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.anneeAccademique.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'annee-accademique/:id/edit',
        component: AnneeAccademiquePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.anneeAccademique.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'annee-accademique/:id/delete',
        component: AnneeAccademiqueDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.anneeAccademique.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
