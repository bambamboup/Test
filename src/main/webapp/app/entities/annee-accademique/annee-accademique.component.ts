import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AnneeAccademique } from './annee-accademique.model';
import { AnneeAccademiqueService } from './annee-accademique.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-annee-accademique',
    templateUrl: './annee-accademique.component.html'
})
export class AnneeAccademiqueComponent implements OnInit, OnDestroy {
anneeAccademiques: AnneeAccademique[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private anneeAccademiqueService: AnneeAccademiqueService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.anneeAccademiqueService.query().subscribe(
            (res: HttpResponse<AnneeAccademique[]>) => {
                this.anneeAccademiques = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInAnneeAccademiques();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: AnneeAccademique) {
        return item.id;
    }
    registerChangeInAnneeAccademiques() {
        this.eventSubscriber = this.eventManager.subscribe('anneeAccademiqueListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
