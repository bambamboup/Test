import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AnneeAccademique } from './annee-accademique.model';
import { AnneeAccademiquePopupService } from './annee-accademique-popup.service';
import { AnneeAccademiqueService } from './annee-accademique.service';

@Component({
    selector: 'jhi-annee-accademique-delete-dialog',
    templateUrl: './annee-accademique-delete-dialog.component.html'
})
export class AnneeAccademiqueDeleteDialogComponent {

    anneeAccademique: AnneeAccademique;

    constructor(
        private anneeAccademiqueService: AnneeAccademiqueService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.anneeAccademiqueService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'anneeAccademiqueListModification',
                content: 'Deleted an anneeAccademique'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-annee-accademique-delete-popup',
    template: ''
})
export class AnneeAccademiqueDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private anneeAccademiquePopupService: AnneeAccademiquePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.anneeAccademiquePopupService
                .open(AnneeAccademiqueDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
