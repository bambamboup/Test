import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { AnneeAccademique } from './annee-accademique.model';
import { AnneeAccademiqueService } from './annee-accademique.service';

@Injectable()
export class AnneeAccademiquePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private anneeAccademiqueService: AnneeAccademiqueService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.anneeAccademiqueService.find(id)
                    .subscribe((anneeAccademiqueResponse: HttpResponse<AnneeAccademique>) => {
                        const anneeAccademique: AnneeAccademique = anneeAccademiqueResponse.body;
                        this.ngbModalRef = this.anneeAccademiqueModalRef(component, anneeAccademique);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.anneeAccademiqueModalRef(component, new AnneeAccademique());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    anneeAccademiqueModalRef(component: Component, anneeAccademique: AnneeAccademique): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.anneeAccademique = anneeAccademique;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
