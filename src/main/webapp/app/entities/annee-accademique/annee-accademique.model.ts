import { BaseEntity } from './../../shared';

export class AnneeAccademique implements BaseEntity {
    constructor(
        public id?: number,
        public nomAnneeAccademique?: string,
        public inscriptions?: BaseEntity[],
    ) {
    }
}
