import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestSharedModule } from '../../shared';
import {
    AnneeAccademiqueService,
    AnneeAccademiquePopupService,
    AnneeAccademiqueComponent,
    AnneeAccademiqueDetailComponent,
    AnneeAccademiqueDialogComponent,
    AnneeAccademiquePopupComponent,
    AnneeAccademiqueDeletePopupComponent,
    AnneeAccademiqueDeleteDialogComponent,
    anneeAccademiqueRoute,
    anneeAccademiquePopupRoute,
} from './';

const ENTITY_STATES = [
    ...anneeAccademiqueRoute,
    ...anneeAccademiquePopupRoute,
];

@NgModule({
    imports: [
        TestSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        AnneeAccademiqueComponent,
        AnneeAccademiqueDetailComponent,
        AnneeAccademiqueDialogComponent,
        AnneeAccademiqueDeleteDialogComponent,
        AnneeAccademiquePopupComponent,
        AnneeAccademiqueDeletePopupComponent,
    ],
    entryComponents: [
        AnneeAccademiqueComponent,
        AnneeAccademiqueDialogComponent,
        AnneeAccademiquePopupComponent,
        AnneeAccademiqueDeleteDialogComponent,
        AnneeAccademiqueDeletePopupComponent,
    ],
    providers: [
        AnneeAccademiqueService,
        AnneeAccademiquePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestAnneeAccademiqueModule {}
