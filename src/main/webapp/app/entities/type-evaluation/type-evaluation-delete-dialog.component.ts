import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TypeEvaluation } from './type-evaluation.model';
import { TypeEvaluationPopupService } from './type-evaluation-popup.service';
import { TypeEvaluationService } from './type-evaluation.service';

@Component({
    selector: 'jhi-type-evaluation-delete-dialog',
    templateUrl: './type-evaluation-delete-dialog.component.html'
})
export class TypeEvaluationDeleteDialogComponent {

    typeEvaluation: TypeEvaluation;

    constructor(
        private typeEvaluationService: TypeEvaluationService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.typeEvaluationService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'typeEvaluationListModification',
                content: 'Deleted an typeEvaluation'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-type-evaluation-delete-popup',
    template: ''
})
export class TypeEvaluationDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private typeEvaluationPopupService: TypeEvaluationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.typeEvaluationPopupService
                .open(TypeEvaluationDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
