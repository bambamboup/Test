import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TypeEvaluation } from './type-evaluation.model';
import { TypeEvaluationService } from './type-evaluation.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-type-evaluation',
    templateUrl: './type-evaluation.component.html'
})
export class TypeEvaluationComponent implements OnInit, OnDestroy {
typeEvaluations: TypeEvaluation[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private typeEvaluationService: TypeEvaluationService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.typeEvaluationService.query().subscribe(
            (res: HttpResponse<TypeEvaluation[]>) => {
                this.typeEvaluations = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTypeEvaluations();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: TypeEvaluation) {
        return item.id;
    }
    registerChangeInTypeEvaluations() {
        this.eventSubscriber = this.eventManager.subscribe('typeEvaluationListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
