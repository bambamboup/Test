import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { TypeEvaluation } from './type-evaluation.model';
import { TypeEvaluationService } from './type-evaluation.service';

@Component({
    selector: 'jhi-type-evaluation-detail',
    templateUrl: './type-evaluation-detail.component.html'
})
export class TypeEvaluationDetailComponent implements OnInit, OnDestroy {

    typeEvaluation: TypeEvaluation;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private typeEvaluationService: TypeEvaluationService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTypeEvaluations();
    }

    load(id) {
        this.typeEvaluationService.find(id)
            .subscribe((typeEvaluationResponse: HttpResponse<TypeEvaluation>) => {
                this.typeEvaluation = typeEvaluationResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTypeEvaluations() {
        this.eventSubscriber = this.eventManager.subscribe(
            'typeEvaluationListModification',
            (response) => this.load(this.typeEvaluation.id)
        );
    }
}
