import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { TypeEvaluationComponent } from './type-evaluation.component';
import { TypeEvaluationDetailComponent } from './type-evaluation-detail.component';
import { TypeEvaluationPopupComponent } from './type-evaluation-dialog.component';
import { TypeEvaluationDeletePopupComponent } from './type-evaluation-delete-dialog.component';

export const typeEvaluationRoute: Routes = [
    {
        path: 'type-evaluation',
        component: TypeEvaluationComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.typeEvaluation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'type-evaluation/:id',
        component: TypeEvaluationDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.typeEvaluation.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const typeEvaluationPopupRoute: Routes = [
    {
        path: 'type-evaluation-new',
        component: TypeEvaluationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.typeEvaluation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'type-evaluation/:id/edit',
        component: TypeEvaluationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.typeEvaluation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'type-evaluation/:id/delete',
        component: TypeEvaluationDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'testApp.typeEvaluation.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
