import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TypeEvaluation } from './type-evaluation.model';
import { TypeEvaluationPopupService } from './type-evaluation-popup.service';
import { TypeEvaluationService } from './type-evaluation.service';

@Component({
    selector: 'jhi-type-evaluation-dialog',
    templateUrl: './type-evaluation-dialog.component.html'
})
export class TypeEvaluationDialogComponent implements OnInit {

    typeEvaluation: TypeEvaluation;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private typeEvaluationService: TypeEvaluationService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.typeEvaluation.id !== undefined) {
            this.subscribeToSaveResponse(
                this.typeEvaluationService.update(this.typeEvaluation));
        } else {
            this.subscribeToSaveResponse(
                this.typeEvaluationService.create(this.typeEvaluation));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<TypeEvaluation>>) {
        result.subscribe((res: HttpResponse<TypeEvaluation>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: TypeEvaluation) {
        this.eventManager.broadcast({ name: 'typeEvaluationListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-type-evaluation-popup',
    template: ''
})
export class TypeEvaluationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private typeEvaluationPopupService: TypeEvaluationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.typeEvaluationPopupService
                    .open(TypeEvaluationDialogComponent as Component, params['id']);
            } else {
                this.typeEvaluationPopupService
                    .open(TypeEvaluationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
