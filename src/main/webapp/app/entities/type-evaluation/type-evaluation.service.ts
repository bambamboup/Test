import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { TypeEvaluation } from './type-evaluation.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<TypeEvaluation>;

@Injectable()
export class TypeEvaluationService {

    private resourceUrl =  SERVER_API_URL + 'api/type-evaluations';

    constructor(private http: HttpClient) { }

    create(typeEvaluation: TypeEvaluation): Observable<EntityResponseType> {
        const copy = this.convert(typeEvaluation);
        return this.http.post<TypeEvaluation>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(typeEvaluation: TypeEvaluation): Observable<EntityResponseType> {
        const copy = this.convert(typeEvaluation);
        return this.http.put<TypeEvaluation>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<TypeEvaluation>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<TypeEvaluation[]>> {
        const options = createRequestOption(req);
        return this.http.get<TypeEvaluation[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TypeEvaluation[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: TypeEvaluation = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<TypeEvaluation[]>): HttpResponse<TypeEvaluation[]> {
        const jsonResponse: TypeEvaluation[] = res.body;
        const body: TypeEvaluation[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to TypeEvaluation.
     */
    private convertItemFromServer(typeEvaluation: TypeEvaluation): TypeEvaluation {
        const copy: TypeEvaluation = Object.assign({}, typeEvaluation);
        return copy;
    }

    /**
     * Convert a TypeEvaluation to a JSON which can be sent to the server.
     */
    private convert(typeEvaluation: TypeEvaluation): TypeEvaluation {
        const copy: TypeEvaluation = Object.assign({}, typeEvaluation);
        return copy;
    }
}
