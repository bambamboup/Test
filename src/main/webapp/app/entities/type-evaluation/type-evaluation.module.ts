import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestSharedModule } from '../../shared';
import {
    TypeEvaluationService,
    TypeEvaluationPopupService,
    TypeEvaluationComponent,
    TypeEvaluationDetailComponent,
    TypeEvaluationDialogComponent,
    TypeEvaluationPopupComponent,
    TypeEvaluationDeletePopupComponent,
    TypeEvaluationDeleteDialogComponent,
    typeEvaluationRoute,
    typeEvaluationPopupRoute,
} from './';

const ENTITY_STATES = [
    ...typeEvaluationRoute,
    ...typeEvaluationPopupRoute,
];

@NgModule({
    imports: [
        TestSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TypeEvaluationComponent,
        TypeEvaluationDetailComponent,
        TypeEvaluationDialogComponent,
        TypeEvaluationDeleteDialogComponent,
        TypeEvaluationPopupComponent,
        TypeEvaluationDeletePopupComponent,
    ],
    entryComponents: [
        TypeEvaluationComponent,
        TypeEvaluationDialogComponent,
        TypeEvaluationPopupComponent,
        TypeEvaluationDeleteDialogComponent,
        TypeEvaluationDeletePopupComponent,
    ],
    providers: [
        TypeEvaluationService,
        TypeEvaluationPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestTypeEvaluationModule {}
