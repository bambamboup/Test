import { BaseEntity } from './../../shared';

export const enum EnumTypeEvaluation {
    'LMD',
    'HYBRIDE'
}

export class TypeEvaluation implements BaseEntity {
    constructor(
        public id?: number,
        public typeEval?: EnumTypeEvaluation,
        public formations?: BaseEntity[],
    ) {
    }
}
