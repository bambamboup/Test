import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { TypeEvaluation } from './type-evaluation.model';
import { TypeEvaluationService } from './type-evaluation.service';

@Injectable()
export class TypeEvaluationPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private typeEvaluationService: TypeEvaluationService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.typeEvaluationService.find(id)
                    .subscribe((typeEvaluationResponse: HttpResponse<TypeEvaluation>) => {
                        const typeEvaluation: TypeEvaluation = typeEvaluationResponse.body;
                        this.ngbModalRef = this.typeEvaluationModalRef(component, typeEvaluation);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.typeEvaluationModalRef(component, new TypeEvaluation());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    typeEvaluationModalRef(component: Component, typeEvaluation: TypeEvaluation): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.typeEvaluation = typeEvaluation;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
