package com.pgi.test.repository;

import com.pgi.test.domain.AnneeAccademique;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AnneeAccademique entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AnneeAccademiqueRepository extends JpaRepository<AnneeAccademique, Long> {

}
