package com.pgi.test.repository;

import com.pgi.test.domain.TiersPayant;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TiersPayant entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TiersPayantRepository extends JpaRepository<TiersPayant, Long> {

}
