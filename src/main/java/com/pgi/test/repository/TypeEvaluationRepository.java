package com.pgi.test.repository;

import com.pgi.test.domain.TypeEvaluation;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TypeEvaluation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TypeEvaluationRepository extends JpaRepository<TypeEvaluation, Long> {

}
