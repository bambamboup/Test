package com.pgi.test.repository;

import com.pgi.test.domain.Journal;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Journal entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JournalRepository extends JpaRepository<Journal, Long> {
    Journal findFirstByOrderByIdDesc();
}
