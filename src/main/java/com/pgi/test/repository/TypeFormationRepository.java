package com.pgi.test.repository;

import com.pgi.test.domain.TypeFormation;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TypeFormation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TypeFormationRepository extends JpaRepository<TypeFormation, Long> {

}
