package com.pgi.test.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.pgi.test.domain.Journal;
import com.pgi.test.repository.JournalRepository;

@Service
@Transactional
public class JournalService {

    private final Logger log = LoggerFactory.getLogger(JournalService.class);

    private final JournalRepository journalRepository;

    public JournalService(JournalRepository journalRepository){
        this.journalRepository = journalRepository;
    }

    public Journal saveJournal(Journal journal){
        long idJournal = journalRepository.findFirstByOrderByIdDesc().getId();
        if(idJournal == 0){
            idJournal = 1;
        }
        journal.setCodeJournal("journal"+idJournal);
        journalRepository.save(journal);
        return journal;
    }

}
