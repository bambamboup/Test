package com.pgi.test.config;

import io.github.jhipster.config.JHipsterProperties;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;

import java.util.concurrent.TimeUnit;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
@AutoConfigureAfter(value = { MetricsConfiguration.class })
@AutoConfigureBefore(value = { WebConfigurer.class, DatabaseConfiguration.class })
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(com.pgi.test.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(com.pgi.test.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.Banque.class.getName(), jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.Fournisseur.class.getName(), jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.TypeFormation.class.getName(), jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.TypeEvaluation.class.getName(), jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.Structure.class.getName(), jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.Structure.class.getName() + ".formations", jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.Journal.class.getName(), jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.Membre.class.getName(), jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.Membre.class.getName() + ".formations", jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.Formation.class.getName(), jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.Formation.class.getName() + ".classes", jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.Formation.class.getName() + ".typeFormations", jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.Formation.class.getName() + ".typeEvaluations", jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.Classe.class.getName(), jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.Classe.class.getName() + ".inscriptions", jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.Nationalite.class.getName(), jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.Nationalite.class.getName() + ".etudiants", jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.TiersPayant.class.getName(), jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.Etudiant.class.getName(), jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.Etudiant.class.getName() + ".inscriptions", jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.AnneeAccademique.class.getName(), jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.AnneeAccademique.class.getName() + ".inscriptions", jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.Inscription.class.getName(), jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.TypeFormation.class.getName() + ".formations", jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.TypeEvaluation.class.getName() + ".formations", jcacheConfiguration);
            cm.createCache(com.pgi.test.domain.Etudiant.class.getName() + ".anneeAccademiques", jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
