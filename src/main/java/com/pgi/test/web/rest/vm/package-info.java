/**
 * View Models used by Spring MVC REST controllers.
 */
package com.pgi.test.web.rest.vm;
