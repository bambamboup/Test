package com.pgi.test.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.pgi.test.domain.AnneeAccademique;

import com.pgi.test.repository.AnneeAccademiqueRepository;
import com.pgi.test.web.rest.errors.BadRequestAlertException;
import com.pgi.test.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing AnneeAccademique.
 */
@RestController
@RequestMapping("/api")
public class AnneeAccademiqueResource {

    private final Logger log = LoggerFactory.getLogger(AnneeAccademiqueResource.class);

    private static final String ENTITY_NAME = "anneeAccademique";

    private final AnneeAccademiqueRepository anneeAccademiqueRepository;

    public AnneeAccademiqueResource(AnneeAccademiqueRepository anneeAccademiqueRepository) {
        this.anneeAccademiqueRepository = anneeAccademiqueRepository;
    }

    /**
     * POST  /annee-accademiques : Create a new anneeAccademique.
     *
     * @param anneeAccademique the anneeAccademique to create
     * @return the ResponseEntity with status 201 (Created) and with body the new anneeAccademique, or with status 400 (Bad Request) if the anneeAccademique has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/annee-accademiques")
    @Timed
    public ResponseEntity<AnneeAccademique> createAnneeAccademique(@Valid @RequestBody AnneeAccademique anneeAccademique) throws URISyntaxException {
        log.debug("REST request to save AnneeAccademique : {}", anneeAccademique);
        if (anneeAccademique.getId() != null) {
            throw new BadRequestAlertException("A new anneeAccademique cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AnneeAccademique result = anneeAccademiqueRepository.save(anneeAccademique);
        return ResponseEntity.created(new URI("/api/annee-accademiques/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /annee-accademiques : Updates an existing anneeAccademique.
     *
     * @param anneeAccademique the anneeAccademique to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated anneeAccademique,
     * or with status 400 (Bad Request) if the anneeAccademique is not valid,
     * or with status 500 (Internal Server Error) if the anneeAccademique couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/annee-accademiques")
    @Timed
    public ResponseEntity<AnneeAccademique> updateAnneeAccademique(@Valid @RequestBody AnneeAccademique anneeAccademique) throws URISyntaxException {
        log.debug("REST request to update AnneeAccademique : {}", anneeAccademique);
        if (anneeAccademique.getId() == null) {
            return createAnneeAccademique(anneeAccademique);
        }
        AnneeAccademique result = anneeAccademiqueRepository.save(anneeAccademique);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, anneeAccademique.getId().toString()))
            .body(result);
    }

    /**
     * GET  /annee-accademiques : get all the anneeAccademiques.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of anneeAccademiques in body
     */
    @GetMapping("/annee-accademiques")
    @Timed
    public List<AnneeAccademique> getAllAnneeAccademiques() {
        log.debug("REST request to get all AnneeAccademiques");
        return anneeAccademiqueRepository.findAll();
        }

    /**
     * GET  /annee-accademiques/:id : get the "id" anneeAccademique.
     *
     * @param id the id of the anneeAccademique to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the anneeAccademique, or with status 404 (Not Found)
     */
    @GetMapping("/annee-accademiques/{id}")
    @Timed
    public ResponseEntity<AnneeAccademique> getAnneeAccademique(@PathVariable Long id) {
        log.debug("REST request to get AnneeAccademique : {}", id);
        AnneeAccademique anneeAccademique = anneeAccademiqueRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(anneeAccademique));
    }

    /**
     * DELETE  /annee-accademiques/:id : delete the "id" anneeAccademique.
     *
     * @param id the id of the anneeAccademique to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/annee-accademiques/{id}")
    @Timed
    public ResponseEntity<Void> deleteAnneeAccademique(@PathVariable Long id) {
        log.debug("REST request to delete AnneeAccademique : {}", id);
        anneeAccademiqueRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
