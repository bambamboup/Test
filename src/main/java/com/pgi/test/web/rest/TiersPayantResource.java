package com.pgi.test.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.pgi.test.domain.TiersPayant;

import com.pgi.test.repository.TiersPayantRepository;
import com.pgi.test.web.rest.errors.BadRequestAlertException;
import com.pgi.test.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TiersPayant.
 */
@RestController
@RequestMapping("/api")
public class TiersPayantResource {

    private final Logger log = LoggerFactory.getLogger(TiersPayantResource.class);

    private static final String ENTITY_NAME = "tiersPayant";

    private final TiersPayantRepository tiersPayantRepository;

    public TiersPayantResource(TiersPayantRepository tiersPayantRepository) {
        this.tiersPayantRepository = tiersPayantRepository;
    }

    /**
     * POST  /tiers-payants : Create a new tiersPayant.
     *
     * @param tiersPayant the tiersPayant to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tiersPayant, or with status 400 (Bad Request) if the tiersPayant has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tiers-payants")
    @Timed
    public ResponseEntity<TiersPayant> createTiersPayant(@RequestBody TiersPayant tiersPayant) throws URISyntaxException {
        log.debug("REST request to save TiersPayant : {}", tiersPayant);
        if (tiersPayant.getId() != null) {
            throw new BadRequestAlertException("A new tiersPayant cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TiersPayant result = tiersPayantRepository.save(tiersPayant);
        return ResponseEntity.created(new URI("/api/tiers-payants/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tiers-payants : Updates an existing tiersPayant.
     *
     * @param tiersPayant the tiersPayant to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tiersPayant,
     * or with status 400 (Bad Request) if the tiersPayant is not valid,
     * or with status 500 (Internal Server Error) if the tiersPayant couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tiers-payants")
    @Timed
    public ResponseEntity<TiersPayant> updateTiersPayant(@RequestBody TiersPayant tiersPayant) throws URISyntaxException {
        log.debug("REST request to update TiersPayant : {}", tiersPayant);
        if (tiersPayant.getId() == null) {
            return createTiersPayant(tiersPayant);
        }
        TiersPayant result = tiersPayantRepository.save(tiersPayant);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tiersPayant.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tiers-payants : get all the tiersPayants.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tiersPayants in body
     */
    @GetMapping("/tiers-payants")
    @Timed
    public List<TiersPayant> getAllTiersPayants() {
        log.debug("REST request to get all TiersPayants");
        return tiersPayantRepository.findAll();
        }

    /**
     * GET  /tiers-payants/:id : get the "id" tiersPayant.
     *
     * @param id the id of the tiersPayant to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tiersPayant, or with status 404 (Not Found)
     */
    @GetMapping("/tiers-payants/{id}")
    @Timed
    public ResponseEntity<TiersPayant> getTiersPayant(@PathVariable Long id) {
        log.debug("REST request to get TiersPayant : {}", id);
        TiersPayant tiersPayant = tiersPayantRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tiersPayant));
    }

    /**
     * DELETE  /tiers-payants/:id : delete the "id" tiersPayant.
     *
     * @param id the id of the tiersPayant to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tiers-payants/{id}")
    @Timed
    public ResponseEntity<Void> deleteTiersPayant(@PathVariable Long id) {
        log.debug("REST request to delete TiersPayant : {}", id);
        tiersPayantRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
