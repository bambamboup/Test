package com.pgi.test.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.pgi.test.domain.TypeEvaluation;

import com.pgi.test.repository.TypeEvaluationRepository;
import com.pgi.test.web.rest.errors.BadRequestAlertException;
import com.pgi.test.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TypeEvaluation.
 */
@RestController
@RequestMapping("/api")
public class TypeEvaluationResource {

    private final Logger log = LoggerFactory.getLogger(TypeEvaluationResource.class);

    private static final String ENTITY_NAME = "typeEvaluation";

    private final TypeEvaluationRepository typeEvaluationRepository;

    public TypeEvaluationResource(TypeEvaluationRepository typeEvaluationRepository) {
        this.typeEvaluationRepository = typeEvaluationRepository;
    }

    /**
     * POST  /type-evaluations : Create a new typeEvaluation.
     *
     * @param typeEvaluation the typeEvaluation to create
     * @return the ResponseEntity with status 201 (Created) and with body the new typeEvaluation, or with status 400 (Bad Request) if the typeEvaluation has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/type-evaluations")
    @Timed
    public ResponseEntity<TypeEvaluation> createTypeEvaluation(@Valid @RequestBody TypeEvaluation typeEvaluation) throws URISyntaxException {
        log.debug("REST request to save TypeEvaluation : {}", typeEvaluation);
        if (typeEvaluation.getId() != null) {
            throw new BadRequestAlertException("A new typeEvaluation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TypeEvaluation result = typeEvaluationRepository.save(typeEvaluation);
        return ResponseEntity.created(new URI("/api/type-evaluations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /type-evaluations : Updates an existing typeEvaluation.
     *
     * @param typeEvaluation the typeEvaluation to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated typeEvaluation,
     * or with status 400 (Bad Request) if the typeEvaluation is not valid,
     * or with status 500 (Internal Server Error) if the typeEvaluation couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/type-evaluations")
    @Timed
    public ResponseEntity<TypeEvaluation> updateTypeEvaluation(@Valid @RequestBody TypeEvaluation typeEvaluation) throws URISyntaxException {
        log.debug("REST request to update TypeEvaluation : {}", typeEvaluation);
        if (typeEvaluation.getId() == null) {
            return createTypeEvaluation(typeEvaluation);
        }
        TypeEvaluation result = typeEvaluationRepository.save(typeEvaluation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, typeEvaluation.getId().toString()))
            .body(result);
    }

    /**
     * GET  /type-evaluations : get all the typeEvaluations.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of typeEvaluations in body
     */
    @GetMapping("/type-evaluations")
    @Timed
    public List<TypeEvaluation> getAllTypeEvaluations() {
        log.debug("REST request to get all TypeEvaluations");
        return typeEvaluationRepository.findAll();
        }

    /**
     * GET  /type-evaluations/:id : get the "id" typeEvaluation.
     *
     * @param id the id of the typeEvaluation to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the typeEvaluation, or with status 404 (Not Found)
     */
    @GetMapping("/type-evaluations/{id}")
    @Timed
    public ResponseEntity<TypeEvaluation> getTypeEvaluation(@PathVariable Long id) {
        log.debug("REST request to get TypeEvaluation : {}", id);
        TypeEvaluation typeEvaluation = typeEvaluationRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(typeEvaluation));
    }

    /**
     * DELETE  /type-evaluations/:id : delete the "id" typeEvaluation.
     *
     * @param id the id of the typeEvaluation to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/type-evaluations/{id}")
    @Timed
    public ResponseEntity<Void> deleteTypeEvaluation(@PathVariable Long id) {
        log.debug("REST request to delete TypeEvaluation : {}", id);
        typeEvaluationRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
