package com.pgi.test.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Formation.
 */
@Entity
@Table(name = "formation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Formation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "code_formation", nullable = false, unique=true)
    private String codeFormation;

    @NotNull
    @Column(name = "nom_formation", nullable = false, unique=true)
    private String nomFormation;

    @NotNull
    @Column(name = "mensualite", precision=10, scale=2, nullable = false)
    private BigDecimal mensualite;

    @OneToMany(mappedBy = "formation")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Classe> classes = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    private Structure departement;

    @ManyToOne(optional = false)
    @NotNull
    private Membre responsable;

    @ManyToOne(optional = false)
    @NotNull
    private TypeFormation typeFormation;

    @ManyToOne(optional = false)
    @NotNull
    private TypeEvaluation typeEvaluatiion;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodeFormation() {
        return codeFormation;
    }

    public Formation codeFormation(String codeFormation) {
        this.codeFormation = codeFormation;
        return this;
    }

    public void setCodeFormation(String codeFormation) {
        this.codeFormation = codeFormation;
    }

    public String getNomFormation() {
        return nomFormation;
    }

    public Formation nomFormation(String nomFormation) {
        this.nomFormation = nomFormation;
        return this;
    }

    public void setNomFormation(String nomFormation) {
        this.nomFormation = nomFormation;
    }

    public BigDecimal getMensualite() {
        return mensualite;
    }

    public Formation mensualite(BigDecimal mensualite) {
        this.mensualite = mensualite;
        return this;
    }

    public void setMensualite(BigDecimal mensualite) {
        this.mensualite = mensualite;
    }

    public Set<Classe> getClasses() {
        return classes;
    }

    public Formation classes(Set<Classe> classes) {
        this.classes = classes;
        return this;
    }

    public Formation addClasse(Classe classe) {
        this.classes.add(classe);
        classe.setFormation(this);
        return this;
    }

    public Formation removeClasse(Classe classe) {
        this.classes.remove(classe);
        classe.setFormation(null);
        return this;
    }

    public void setClasses(Set<Classe> classes) {
        this.classes = classes;
    }

    public Structure getDepartement() {
        return departement;
    }

    public Formation departement(Structure structure) {
        this.departement = structure;
        return this;
    }

    public void setDepartement(Structure structure) {
        this.departement = structure;
    }

    public Membre getResponsable() {
        return responsable;
    }

    public Formation responsable(Membre membre) {
        this.responsable = membre;
        return this;
    }

    public void setResponsable(Membre membre) {
        this.responsable = membre;
    }

    public TypeFormation getTypeFormation() {
        return typeFormation;
    }

    public Formation typeFormation(TypeFormation typeFormation) {
        this.typeFormation = typeFormation;
        return this;
    }

    public void setTypeFormation(TypeFormation typeFormation) {
        this.typeFormation = typeFormation;
    }

    public TypeEvaluation getTypeEvaluatiion() {
        return typeEvaluatiion;
    }

    public Formation typeEvaluatiion(TypeEvaluation typeEvaluation) {
        this.typeEvaluatiion = typeEvaluation;
        return this;
    }

    public void setTypeEvaluatiion(TypeEvaluation typeEvaluation) {
        this.typeEvaluatiion = typeEvaluation;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Formation formation = (Formation) o;
        if (formation.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), formation.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Formation{" +
            "id=" + getId() +
            ", codeFormation='" + getCodeFormation() + "'" +
            ", nomFormation='" + getNomFormation() + "'" +
            ", mensualite=" + getMensualite() +
            "}";
    }
}
