package com.pgi.test.domain.enumeration;

/**
 * The EnumTypeFormation enumeration.
 */
public enum EnumTypeFormation {
    FPCT, FPCE
}
