package com.pgi.test.domain.enumeration;

/**
 * The EnumGenre enumeration.
 */
public enum EnumGenre {
    MASCULIN, FEMININ
}
