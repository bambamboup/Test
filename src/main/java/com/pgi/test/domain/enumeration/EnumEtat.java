package com.pgi.test.domain.enumeration;

/**
 * The EnumEtat enumeration.
 */
public enum EnumEtat {
    ENCOURS, ABANDON
}
