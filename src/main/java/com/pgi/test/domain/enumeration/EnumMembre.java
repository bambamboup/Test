package com.pgi.test.domain.enumeration;

/**
 * The EnumMembre enumeration.
 */
public enum EnumMembre {
    PATS, PER
}
