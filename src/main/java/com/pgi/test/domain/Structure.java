package com.pgi.test.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Structure.
 */
@Entity
@Table(name = "structure")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Structure implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "code_structure", nullable = false)
    private String codeStructure;

    @NotNull
    @Column(name = "nom_structure", nullable = false)
    private String nomStructure;

    @OneToMany(mappedBy = "departement")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Formation> formations = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodeStructure() {
        return codeStructure;
    }

    public Structure codeStructure(String codeStructure) {
        this.codeStructure = codeStructure;
        return this;
    }

    public void setCodeStructure(String codeStructure) {
        this.codeStructure = codeStructure;
    }

    public String getNomStructure() {
        return nomStructure;
    }

    public Structure nomStructure(String nomStructure) {
        this.nomStructure = nomStructure;
        return this;
    }

    public void setNomStructure(String nomStructure) {
        this.nomStructure = nomStructure;
    }

    public Set<Formation> getFormations() {
        return formations;
    }

    public Structure formations(Set<Formation> formations) {
        this.formations = formations;
        return this;
    }

    public Structure addFormation(Formation formation) {
        this.formations.add(formation);
        formation.setDepartement(this);
        return this;
    }

    public Structure removeFormation(Formation formation) {
        this.formations.remove(formation);
        formation.setDepartement(null);
        return this;
    }

    public void setFormations(Set<Formation> formations) {
        this.formations = formations;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Structure structure = (Structure) o;
        if (structure.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), structure.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Structure{" +
            "id=" + getId() +
            ", codeStructure='" + getCodeStructure() + "'" +
            ", nomStructure='" + getNomStructure() + "'" +
            "}";
    }
}
