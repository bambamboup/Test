package com.pgi.test.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Journal.
 */
@Entity
@Table(name = "journal")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Journal implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "code_journal")
    private String codeJournal;

    @NotNull
    @Column(name = "nom_journal", nullable = false, unique=true)
    private String nomJournal;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodeJournal() {
        return codeJournal;
    }

    public Journal codeJournal(String codeJournal) {
        this.codeJournal = codeJournal;
        return this;
    }

    public void setCodeJournal(String codeJournal) {
        this.codeJournal = codeJournal;
    }

    public String getNomJournal() {
        return nomJournal;
    }

    public Journal nomJournal(String nomJournal) {
        this.nomJournal = nomJournal;
        return this;
    }

    public void setNomJournal(String nomJournal) {
        this.nomJournal = nomJournal;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Journal journal = (Journal) o;
        if (journal.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), journal.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Journal{" +
            "id=" + getId() +
            ", codeJournal='" + getCodeJournal() + "'" +
            ", nomJournal='" + getNomJournal() + "'" +
            "}";
    }
}
