package com.pgi.test.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.pgi.test.domain.enumeration.EnumNivLMD;

/**
 * A Classe.
 */
@Entity
@Table(name = "classe")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Classe implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "code_classe", nullable = false)
    private String codeClasse;

    @NotNull
    @Column(name = "libelle", nullable = false)
    private String libelle;

    @NotNull
    @Column(name = "terminal", nullable = false)
    private Boolean terminal;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "niv_lmd", nullable = false)
    private EnumNivLMD nivLMD;

    @NotNull
    @Column(name = "frais_inscription", precision=10, scale=2, nullable = false)
    private BigDecimal fraisInscription;

    @NotNull
    @Column(name = "entrant", nullable = false)
    private Boolean entrant;

    @NotNull
    @Column(name = "sortant", nullable = false)
    private Boolean sortant;

    @OneToMany(mappedBy = "classe")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Inscription> inscriptions = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    private Formation formation;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodeClasse() {
        return codeClasse;
    }

    public Classe codeClasse(String codeClasse) {
        this.codeClasse = codeClasse;
        return this;
    }

    public void setCodeClasse(String codeClasse) {
        this.codeClasse = codeClasse;
    }

    public String getLibelle() {
        return libelle;
    }

    public Classe libelle(String libelle) {
        this.libelle = libelle;
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Boolean isTerminal() {
        return terminal;
    }

    public Classe terminal(Boolean terminal) {
        this.terminal = terminal;
        return this;
    }

    public void setTerminal(Boolean terminal) {
        this.terminal = terminal;
    }

    public EnumNivLMD getNivLMD() {
        return nivLMD;
    }

    public Classe nivLMD(EnumNivLMD nivLMD) {
        this.nivLMD = nivLMD;
        return this;
    }

    public void setNivLMD(EnumNivLMD nivLMD) {
        this.nivLMD = nivLMD;
    }

    public BigDecimal getFraisInscription() {
        return fraisInscription;
    }

    public Classe fraisInscription(BigDecimal fraisInscription) {
        this.fraisInscription = fraisInscription;
        return this;
    }

    public void setFraisInscription(BigDecimal fraisInscription) {
        this.fraisInscription = fraisInscription;
    }

    public Boolean isEntrant() {
        return entrant;
    }

    public Classe entrant(Boolean entrant) {
        this.entrant = entrant;
        return this;
    }

    public void setEntrant(Boolean entrant) {
        this.entrant = entrant;
    }

    public Boolean isSortant() {
        return sortant;
    }

    public Classe sortant(Boolean sortant) {
        this.sortant = sortant;
        return this;
    }

    public void setSortant(Boolean sortant) {
        this.sortant = sortant;
    }

    public Set<Inscription> getInscriptions() {
        return inscriptions;
    }

    public Classe inscriptions(Set<Inscription> inscriptions) {
        this.inscriptions = inscriptions;
        return this;
    }

    public Classe addInscription(Inscription inscription) {
        this.inscriptions.add(inscription);
        inscription.setClasse(this);
        return this;
    }

    public Classe removeInscription(Inscription inscription) {
        this.inscriptions.remove(inscription);
        inscription.setClasse(null);
        return this;
    }

    public void setInscriptions(Set<Inscription> inscriptions) {
        this.inscriptions = inscriptions;
    }

    public Formation getFormation() {
        return formation;
    }

    public Classe formation(Formation formation) {
        this.formation = formation;
        return this;
    }

    public void setFormation(Formation formation) {
        this.formation = formation;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Classe classe = (Classe) o;
        if (classe.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), classe.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Classe{" +
            "id=" + getId() +
            ", codeClasse='" + getCodeClasse() + "'" +
            ", libelle='" + getLibelle() + "'" +
            ", terminal='" + isTerminal() + "'" +
            ", nivLMD='" + getNivLMD() + "'" +
            ", fraisInscription=" + getFraisInscription() +
            ", entrant='" + isEntrant() + "'" +
            ", sortant='" + isSortant() + "'" +
            "}";
    }
}
