package com.pgi.test.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.pgi.test.domain.enumeration.EnumMembre;

/**
 * A Membre.
 */
@Entity
@Table(name = "membre")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Membre implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "matricule", nullable = false)
    private String matricule;

    @NotNull
    @Column(name = "telephone", nullable = false)
    private String telephone;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type_membre", nullable = false)
    private EnumMembre typeMembre;

    @OneToOne(optional = false)
    @NotNull
    @JoinColumn(unique = true)
    private User user;

    @OneToMany(mappedBy = "responsable")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Formation> formations = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMatricule() {
        return matricule;
    }

    public Membre matricule(String matricule) {
        this.matricule = matricule;
        return this;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getTelephone() {
        return telephone;
    }

    public Membre telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public EnumMembre getTypeMembre() {
        return typeMembre;
    }

    public Membre typeMembre(EnumMembre typeMembre) {
        this.typeMembre = typeMembre;
        return this;
    }

    public void setTypeMembre(EnumMembre typeMembre) {
        this.typeMembre = typeMembre;
    }

    public User getUser() {
        return user;
    }

    public Membre user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Formation> getFormations() {
        return formations;
    }

    public Membre formations(Set<Formation> formations) {
        this.formations = formations;
        return this;
    }

    public Membre addFormation(Formation formation) {
        this.formations.add(formation);
        formation.setResponsable(this);
        return this;
    }

    public Membre removeFormation(Formation formation) {
        this.formations.remove(formation);
        formation.setResponsable(null);
        return this;
    }

    public void setFormations(Set<Formation> formations) {
        this.formations = formations;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Membre membre = (Membre) o;
        if (membre.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), membre.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Membre{" +
            "id=" + getId() +
            ", matricule='" + getMatricule() + "'" +
            ", telephone='" + getTelephone() + "'" +
            ", typeMembre='" + getTypeMembre() + "'" +
            "}";
    }
}
