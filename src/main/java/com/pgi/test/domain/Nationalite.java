package com.pgi.test.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Nationalite.
 */
@Entity
@Table(name = "nationalite")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Nationalite implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nom_nationalite", nullable = false)
    private String nomNationalite;

    @OneToMany(mappedBy = "nationalite")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Etudiant> etudiants = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomNationalite() {
        return nomNationalite;
    }

    public Nationalite nomNationalite(String nomNationalite) {
        this.nomNationalite = nomNationalite;
        return this;
    }

    public void setNomNationalite(String nomNationalite) {
        this.nomNationalite = nomNationalite;
    }

    public Set<Etudiant> getEtudiants() {
        return etudiants;
    }

    public Nationalite etudiants(Set<Etudiant> etudiants) {
        this.etudiants = etudiants;
        return this;
    }

    public Nationalite addEtudiant(Etudiant etudiant) {
        this.etudiants.add(etudiant);
        etudiant.setNationalite(this);
        return this;
    }

    public Nationalite removeEtudiant(Etudiant etudiant) {
        this.etudiants.remove(etudiant);
        etudiant.setNationalite(null);
        return this;
    }

    public void setEtudiants(Set<Etudiant> etudiants) {
        this.etudiants = etudiants;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Nationalite nationalite = (Nationalite) o;
        if (nationalite.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), nationalite.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Nationalite{" +
            "id=" + getId() +
            ", nomNationalite='" + getNomNationalite() + "'" +
            "}";
    }
}
